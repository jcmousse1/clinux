#!/bin/bash
#
# 
#############################################################################
##### Script permettant de joindre un client Debian Wheezy au serveur SE3
# Montage des /home/user, de /var/se3/Classes et de /var/se3/Docs en cifs
# 
# Testé en production avec Debian Wheezy (Lxde) sur se3 squeeze
#############################################################################

DATERAPPORT=$(date +%F+%0kh%0M)
rm -f /root/SE3_rapport_integration_*

{
# Section a completer avec vos parametres !
# Il faut lancer le script rejoint_autoconfig.sh sur le se3, puis exporter ce script sur les clients
SE3_SERVER="##se3server##" # nom dns du se3 (hostname)
SE3_IP="##se3ip##" # ip du se3
DOMAIN="##se3domain##" # Nom du domaine samba
LDAP_SUFFIX="##se3suffix##" # voir dans /etc/samba/smb.conf
LDAP_ADMIN_DN="##se3admindn##" # voir dans /etc/samba/smb.conf
LDAP_SERVER="##se3ip##" # ip de l'annuaire ldap
PASS_LDAP="##se3passldap##" # mdp de l'administrateur du ldap
SERVINV="http://fi-inv.ac-clermont.fr/glpi/plugins/fusioninventory/" # spécifique académie de Clermont-Fd
TAG="##se3rne##P" # RNE + 'P' (péda) ou 'A' (administratif)
RNE="##se3rne##"
NTPSERVERS="ntp.ac-creteil.fr" # adresse d'un serveur de temps : ex ntp.ac-clermont.fr
IOCHARSET="utf8" # normalement utf8 - voir /etc/samba/smb.conf
TLS="0" # laisser a 0
DATESCRIPT="20151004"
VERSION="1.27"

##########################################
# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "7.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 7 (Wheezy)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 7 (Wheezy) détectée... on poursuit."
fi

# Test renseignement variables fondamentales
if [ -z "${SE3_SERVER}" -o "${SE3_SERVER:0:1}" = "#" ]; then
echo " Erreur: la variable SE3_SERVER n'est pas renseignee."
exit
fi

if [ -z "${SE3_IP}" -o "${SE3_IP:0:1}" = "#" ]; then
echo " Erreur: la variable SE3_IP n'est pas renseignee."
exit
fi

if [ -z "$LDAP_SUFFIX" -o "$LDAP_SUFFIX" = "#" ]; then
echo " Erreur: la variable BASE_DN n'est pas renseignee."
exit
fi

if [ -z "${LDAP_SERVER}" -o "${LDAP_SERVER:0:1}" = "#" ]; then
echo " Erreur: la variable LDAP_SERVER n'est pas renseignee."
exit
fi

echo "La version du script utilise est $VERSION datée du $DATESCRIPT"

TEST_ROCHE=$(cat /etc/profile | grep "139.253")
if [ ! -n "$TEST_ROCHE" ]; then
echo -e "$JAUNE" "Le proxy http n'est pas renseigné dans /etc/profile" ; tput sgr0
	# On pose la question
	echo -e "$ROUGE" "Lieu d'intégration : etes-vous au lycée Roche-Arnaud ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Intégration au lycée Roche-Arnaud"
				ROCHE=o
				break
				;;
				2|n)
				echo "Intégration dans un autre établissement"
				ROCHE=n
				break
				;;
			esac
		done

		if [ $ROCHE = "o" ] ; then
		# On declare le proxy http dans /etc/profile si on installe depuis Roche-Arnaud,
		# Puis on sort du script : il faut se relogger pour prendre le proxy en compte
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		echo "export http_proxy=\"http://172.17.139.253:3128\"" >> /etc/profile
		echo "export https_proxy=\"https://172.17.139.253:3128\"" >> /etc/profile
		echo "Le proxy http a été renseigné dans /etc/profile."
		echo "Le script va s'interrompre..."
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 3
		exit
		else
		# Sinon on continue
		echo "On poursuit..." 
		fi
else
# On continue
echo "Suite" 
fi

# Configuration de l'integration de la machine : hostname, inventaire, proxy, parc
		echo "Configuration des parametres de l'intégration :"
		echo ""
		echo "Quel nom choisissez-vous pour cette machine?"
		read NOMMACH
		echo "Le nom choisi pour la machine est $NOMMACH."
		echo ""
		echo "Dans quel parc sera intégrée cette machine ?"
		echo "Attention : seuls choix possibles :"
		echo "cdi, sprofs"
		read PARK

echo $PARK > /etc/parc.txt
chmod 744 /etc/parc.txt

		echo "A la fin de l'installation, voulez-vous une remontée des informations dans l'inventaire ?"
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Vous avez choisi la remontée des infos de la machine dans l'inventaire."
				OCS=o
				break
				;;
				2|n)
				echo "Vous avez refuse la remontée des infos de la machine dans l'inventaire."
				echo ""
				OCS=n
				break
				;;
			esac
		done
		
		echo "Voulez-vous configurer un proxy (Amon) sur la machine ?"
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Vous avez choisi de configurer un proxy sur la machine."
				CONFPROXY=o
				break
				;;
				2|n)
				echo "Vous avez refuse de configurer un proxy sur la machine."
				echo ""
				CONFPROXY=n
				break
				;;
			esac
		done

if [ $CONFPROXY = "o" ] ; then
echo -e "$JAUNE" "Adresse du proxy (Amon) de l'établissement où sera installe le pc :" ; tput sgr0
		echo ""
		echo "Donnez l'adresse IP et le port du proxy HTTP sous la forme 172.17.139.253:3128"
		read PROXY
		echo -e "$VERT" "L'adresse IP du proxy est $PROXY" ; tput sgr0
sed -i '/export/d' /etc/profile
sed -i '/Configuration/d' /etc/profile
echo "#Configuration du proxy pour l'établissement
export http_proxy=\"http://$PROXY\"
export https_proxy=\"https://$PROXY\"" >> /etc/profile
fi
		
		echo "Voulez-vous configurer un cache apt sur la machine ?"
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Vous avez choisi de configurer un cache apt sur la machine."
				CACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt à configurer sur la machine."
				echo ""
				CACHE=n
				break
				;;
			esac
		done

if [ $CACHE = "o" ] ; then
# Definition de l'adresse du cache apt de l'etablissement
		echo -e "$JAUNE" "Adresse du cache APT de l'etablissement :" ; tput sgr0
		echo ""
		echo "Donnez l'adresse IP et le port du cache APT sous la forme 172.17.139.251:3142"
		read APTCACHE
		echo -e "$VERT" "L'adresse IP du cache apt est $APTCACHE" ; tput sgr0
echo "Acquire::http { Proxy \"http://$APTCACHE\"; };"> /etc/apt/apt.conf.d/99proxy
else
	if [ $CONFPROXY = "o" ] ; then
	echo "Acquire::http::Proxy \"http://$PROXY\";" > /etc/apt/apt.conf.d/99proxy
	else
	echo "#Configuration sans cache" > /etc/apt/apt.conf.d/99proxy
	fi
fi

# Mise a jour de la machine
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=high

echo "Mise a jour de la machine..."

# Resolution du probleme de lock
if [ -e "/var/lib/dpkg/lock" ]; then
	rm -f /var/lib/dpkg/lock
fi

# On lance une maj
apt-get update
apt-get -y dist-upgrade

# Installation des paquets necessaires
echo "Installation des paquets necessaires:"
echo "Ne rien remplir, les fichiers seront configures/modifies automatiquement apres..."
apt-get install -y --force-yes libnss-ldap lsof libpam-mount cifs-utils samba ntpdate ssh aptitude ldap-utils cron-apt numlockx dkms

# Configuration des fichiers
echo "Configuration des fichiers pour SambaEdu 3..."
echo ""

# Configuration du fichier /etc/hosts
echo "Configuration du fichier /etc/hosts"

cp /etc/hosts /etc/hosts_sauve_$DATERAPPORT
OK_SE3=`cat /etc/hosts | grep $SE3_SERVER`
if [ -z "$OK_SE3" ]; then
	echo "$SE3_IP	$SE3_SERVER" >> /etc/hosts
fi

ANCIENNOM=$(hostname)
cp /etc/hosts /etc/hosts_sauve_$DATERAPPORT
#sed -i 's/'$ANCIENNOM'/'$NOMMACH'/g' /etc/hosts
sed -i 's/^127.0.1.1.*/127.0.1.1\	'$NOMMACH'.'$RNE'.ac-clermont.fr\	'$NOMMACH'/g' /etc/hosts

echo "Configuration du fichier /etc/hostname"
cp /etc/hostname /etc/hostname_sauve_$DATERAPPORT
echo "$NOMMACH" > /etc/hostname

# Configuration du fichier /etc/nsswitch.conf
echo "Configuration du fichier /etc/nsswitch.conf"

cp /etc/nsswitch.conf /etc/nsswitch.conf_sauve_$DATERAPPORT
echo "
# /etc/nsswitch.conf
# Configuration pour SambaEdu3

passwd:         files ldap
group:          files ldap
shadow:         files ldap

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis" > /etc/nsswitch.conf

# Configuration du fichier /etc/libnss-ldap.conf
echo "Configuration du fichier /etc/libnss-ldap.conf"

cp /etc/libnss-ldap.conf /etc/libnss-ldap.conf_sauve_$DATERAPPORT
echo "
# /etc/libnss-ldap.conf
# Configuration pour SambaEdu3

host $LDAP_SERVER
base $LDAP_SUFFIX
uri ldap://$LDAP_SERVER
ldap_version 3
rootbinddn $LDAP_ADMIN_DN

# The port.
# Optional: default is 389.
#port 389

bind_policy soft" > /etc/libnss-ldap.conf


# Configuration du fichier /etc/pam_ldap.conf
echo "Configuration du fichier /etc/pam_ldap.conf"

cp /etc/pam_ldap.conf /etc/pam_ldap.conf_sauve_$DATERAPPORT
echo "
# /etc/pam_ldap.conf
# Configuration pour SambaEdu3

host $LDAP_SERVER
base $LDAP_SUFFIX
uri ldap://$LDAP_SERVER
ldap_version 3

# The port.
# Optional: default is 389.
#port 389

bind_policy soft" > /etc/pam_ldap.conf

# Configuration du fichier /etc/libnss-ldap.secret
echo "Configuration du fichier /etc/libnss-ldap.secret "
echo -n "$PASS_LDAP" > /etc/libnss-ldap.secret

# Configuration du fichier /etc/pam.d/common-auth
echo "Configuration du fichier /etc/pam.d/common-auth"

cp /etc/pam.d/common-auth /etc/pam.d/common-auth_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-auth
# Configuration pour SambaEdu3

auth	[success=2 default=ignore]	pam_unix.so nullok_secure
auth	[success=1 default=ignore]	pam_ldap.so use_first_pass
auth	requisite			pam_deny.so
auth	required			pam_permit.so
auth	optional	pam_mount.so
auth	optional	pam_group.so" > /etc/pam.d/common-auth

# Configuration du fichier /etc/pam.d/common-session
echo "Configuration du fichier /etc/pam.d/common-session"

cp /etc/pam.d/common-session /etc/pam.d/common-session_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-session
# Configuration pour SambaEdu3

session	[default=1]			pam_permit.so
session	requisite			pam_deny.so
session	required			pam_permit.so
session	required	pam_mkhomedir.so	skel=/etc/skel/	umask=0022 
session	optional	pam_mount.so 
session	optional			pam_ldap.so" > /etc/pam.d/common-session

# Ajout de admin a la liste des sudoers avec pouvoir de root
#cp -a /etc/sudoers /etc/sudoers_sauve_$DATERAPPORT
#sed -i '/root /a\admin	ALL=(ALL) ALL' /etc/sudoers

# Configuration du fichier /etc/security/group.conf
echo "Configuration du fichier /etc/security/group.conf"
cp /etc/security/group.conf /etc/security/group.conf_sauve_$DATERAPPORT
echo "
# /etc/security/group.conf
# Configuration pour SambaEdu3
gdm3;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev
xdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev" > /etc/security/group.conf

# Configuration du fichier /etc/samba/smb.conf
echo "Configuration du fichier /etc/samba/smb.conf"
mv /etc/samba/smb.conf /etc/samba/smb.conf.ori
echo "[global]
domain master = False
local master = no
lm announce = False
preferred master = no
workgroup = $DOMAIN
#smb ports = 445
netbios name = $NOMMACH" > /etc/samba/smb.conf

# Desactivation de l affichage de la liste des utilisateurs anterieurs (gdm3 uniquement)
sed -i 's/# disable-user-list=true/disable-user-list=true/g' /etc/gdm3/greeter.gsettings

# Creation des points de montage
mkdir -p /media/Classes
mkdir -p /media/Partages

# Configuration du fichier /etc/security/pam_mount.conf.xml
cp /etc/security/pam_mount.conf.xml /etc/security/pam_mount.conf.xml_sauve_$DATERAPPORT
echo "Configuration du fichier /etc/security/pam_mount.conf.xml"

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>

<!--
/etc/security/pam_mount.conf.xml
Configuration pour SambaEdu3
-->

<pam_mount>
  <debug enable=\"1\" />
  <logout wait=\"0\" hup=\"0\" term=\"0\" kill=\"0\" />
  <mkmountpoint enable=\"1\" remove=\"true\" />

  <umount>umount %(MNTPT)</umount>

  <volume
    pgrp=\"lcs-users\"
    fstype=\"cifs\"
    server=\"$SE3_IP\"
    path=\"%(USER)\" 
    mountpoint=\"/home/%(USER)\"
    options=\"user=%(USER),nobrl,serverino,iocharset=utf8,sec=ntlmv2\"
  />

  <volume
    pgrp=\"lcs-users\"
    fstype=\"cifs\"
    server=\"$SE3_IP\"
    path=\"Classes\" 
    mountpoint=\"/media/Classes\"
    options=\"user=%(USER),nobrl,serverino,iocharset=utf8,sec=ntlmv2\"
  />

  <volume
    pgrp=\"lcs-users\"
    fstype=\"cifs\"
    server=\"$SE3_IP\"
    path=\"Docs\" 
    mountpoint=\"/media/Partages\"
    options=\"user=%(USER),nobrl,serverino,iocharset=utf8,sec=ntlmv2\"
  />

<msg-authpw>Mot de passe :</msg-authpw>
<msg-sessionpw>Mot de passe :</msg-sessionpw>

</pam_mount>" > /etc/security/pam_mount.conf.xml

killall trackerd 2>/dev/null
killall bluetooth-applet 2>/dev/null

# Modifications pour le montage sur les clients d'un partage du se3
if [ ! -e /mnt/netlogon ] ; then
mkdir -p /mnt/netlogon
chown -R admin:admins /mnt/netlogon
chmod -R 755 /mnt/netlogon
fi

# Création d'une entrée dans /etc/fstab pour monter un répertoire partagé du se3
if [ -e /mnt/netlogon ] ; then
sed -i '/linux/d' /etc/fstab
sed -i '/^$/d' /etc/fstab
echo "//$SE3_IP/netlogon-linux  /mnt/netlogon   cifs    guest,nobrl,serverino,iocharset=utf8,sec=ntlmv2 0       0" >> /etc/fstab
fi

# On teste si lxde est installé
lxde=/etc/xdg/lxsession
if [ -e $lxde ]; then
# Lancement des scripts "unefois" au chargement de gdm
sed -i '/exit 0/d' /etc/gdm3/Init/Default
sed -i '/netlogon/d' /etc/gdm3/Init/Default
sed -i '/unefois/d' /etc/gdm3/Init/Default
echo "sh /mnt/netlogon/conex/init_unefois.sh
exit 0" >> /etc/gdm3/Init/Default
chmod +x /etc/gdm3/Init/Default

# Configuration de l'ouverture de session
# Creations de liens sur le bureau si on utilise lxde
echo "#!/bin/bash
#
# Note that any setup should come before the sessreg command as
# that must be 'exec'ed for the pid to be correct (sessreg uses the parent
# pid)
# Note that output goes into the .xsession-errors file for easy debugging
#
PATH=\"/usr/bin:\$PATH\"
sh /mnt/netlogon/conex/createlinks.sh &
exit 0" > /etc/gdm3/PreSession/Default
chmod +x /etc/gdm3/PreSession/Default

# Configuration de la fermeture de session
# kill des processus de l'utilisateur courant et effacement du lock firefox si présent
# Effacement des raccourcis (liens symboliques) créés a l'ouverture de session
echo "#!/bin/bash
sh /mnt/netlogon/conex/deletelinks.sh
exit 0" > /etc/gdm3/PostSession/Default
chmod +x /etc/gdm3/PostSession/Default

fi

# Configuration du fichier /etc/default/ntpdate
echo "Configuration du fichier /etc/default/ntpdate"

cp /etc/default/ntpdate /etc/default/ntpdate_sauve_$DATERAPPORT
echo "
# /etc/default/ntpdate
# Configuration pour SambaEdu3

NTPSERVERS=\"$NTPSERVERS\"

# additional options for ntpdate
# $NTPOPTIONS" > /etc/default/ntpdate

# On recupere la cle publique du serveur
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cd /root/.ssh
if [ -f "authorized_keys" ];then
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys_se3 $SE3_IP:909/authorized_keys
cat authorized_keys_se3 >> authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
else
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys $SE3_IP:909/authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
fi
chmod 400 /root/.ssh/authorized_keys*
cd

# Configuration de /etc/cron-apt pour n'installer que les maj de sécurité
echo "Configuration de /etc/cron-apt"
less /etc/apt/sources.list|grep security > /etc/apt/security.sources.list
cp /etc/cron-apt/config /etc/cron-apt/config_sauve_$DATERAPPORT

echo "# /etc/cron-apt/config
# Configuration pour SambaEdu3
OPTIONS=\"-o quiet=1 -o Dir::Etc::SourceList=/etc/apt/security.sources.list\"" > /etc/cron-apt/config

echo "# Configuration pour Samba Edu3
dist-upgrade -y -o APT::Get::Show-Upgraded=true" > /etc/cron-apt/action.d/5-install
ln -s /usr/sbin/cron-apt /etc/cron.daily/cron-apt

dpkg-reconfigure -plow unattended-upgrades

# Configuration de fusion_inventory
if [ $OCS = "o" ] ; then
echo "Configuration de fusion_inventory"
aptitude -y purge ocsinventory-agent
aptitude -y install fusioninventory-agent
cp /etc/default/fusioninventory-agent /etc/default/fusioninventory-agent.ori
sed -i 's/MODE=cron/MODE=daemon/g' /etc/default/fusioninventory-agent
echo "server=$SERVINV">/etc/fusioninventory/agent.cfg
echo "tag=$TAG">>/etc/fusioninventory/agent.cfg
	if [ $CONFPROXY = "o" ] ; then
echo "proxy = $PROXY">> /etc/fusioninventory/agent.cfg
	fi
/etc/init.d/fusioninventory-agent restart 
fi

# Suppression de gnome-network-manager
echo "Suppression de network-manager"
apt-get remove --purge -y network-manager*
apt-get -y autoremove
apt-get clean
apt-get autoclean

# Suppression des regles udev pour eviter le renommage de l'interface ethx
echo "Suppression des regles udev"
rm -rf /etc/udev/rules.d/70-persistent-*

# Configuration de /etc/network/interfaces pour le dhcp
echo "Configuration de /etc/network/interfaces"
echo "# The loopback network interface
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp" > /etc/network/interfaces

# Datage du script (temoin de passage dans /root)
cd /root
rm -f integration_*
rm -f SE3_rapport_integration*
echo "Script d'integration version $VERSION du $DATESCRIPT passe le $DATERAPPORT" >> /root/integration_$VERSION_$DATESCRIPT

# Fin de la configuration
echo "Fin de l'installation."

echo "ATTENTION : Seul les comptes ayant un shell peuvent se connecter"
echo ""
echo "Vous devez configurer les locale pour etre compatible avec Se3"
echo "Il faut redemarrer la machine pour prendre en compte la nouvelle configuration"


export DEBIAN_FRONTEND=dialog

} | tee /root/SE3_rapport_integration_wheezy_$DATERAPPORT
