#!/bin/bash
#
# ************************************************************************
# chg_uuid_swap.sh
# Script de correction de l'uuid de la partition swap sur les clients linux
# 20151211
# ************************************************************************

# On rend le script "crétin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# On recupere l'id du peripherique
#blkid | grep 'swap' | cut -f1 | awk '{ print $1}' > /etc/periph
#sed -i 's/://g' /etc/periph
#perswap=$(cat /etc/periph)
#echo $perswap
echo "Correction de l'uuid de la partition swap après clonage..."
# On recupère l'uuid correct
blkid | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/swapuuid
sed -i 's/"//g' /etc/swapuuid
swuid=$(cat /etc/swapuuid)
echo "identifiant correct : $swuid"

# On recupere l'uuid contenu dans /etc/fstab
cat /etc/fstab | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/wronguuid
sed -i 's/#//g' /etc/wronguuid
sed -i '/^$/d' /etc/wronguuid
wrong=$(cat /etc/wronguuid)
echo "identifiant à corriger : $wrong"

# On remplace par la bonne valeur
sed -i 's/'$wrong'/'$swuid'/g' /etc/fstab
#swaplabel -U $swuid $perswap
echo "Correction terminée !"
echo " Redémarrez le client."
