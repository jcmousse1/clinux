#!/bin/bash
#
###########################################################
# copie_clepub_veyon.sh
# Ce script est à lancer sur les clients veyon
# Il recupère la clé publique "teacher" du poste "maitre" veyon
# 20180327
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

cle=key
repcle=/etc/veyon/keys/public/teacher

echo -e "$JAUNE"
echo "###########################################################################"
echo ""
echo "Voulez-vous configurer veyon (client) sur ce poste ?"
echo "Il faudra indiquer l'adresse ip du poste maitre veyon et son mdp root."
echo ""
echo "###########################################################################"
echo -e "$COLTXT"
echo -e "$COLINFO"
read -p "Que voulez-vous faire ?

1 (je veux configurer veyon sur ce client)
2 (non, je veux sortir !) : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du poste veyon-master : "
                                echo -e "$COLTXT"
                                read IPMAST
				cd /etc
				rm -rf veyon
				cd
				mkdir -p $repcle
				#mv $repcle/$cle $repcle/cle_$DATE1
				cd $repcle
				scp root@$IPMAST:$repcle/$cle . || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie de la clé..."
					echo "Vérifier l'adresse ip du poste veyon-master puis relancez le script."
                                        echo -e "$COLTXT"
                                else	
					# Ajustement des droits et message
					cd /etc
					chmod -R 777 veyon/
                                        echo -e "$VERT"
                                        echo "La clé a été copiée depuis le poste veyon-master $IPMAST"
					echo -e "$COLTXT"
					# Neutralisation de master et de configurator sur le poste eleve
					chmod -x /usr/bin/veyon-master
					chmod -x /usr/bin/veyon-configurator
					cd /usr/share/applications
					mv veyon-master.desktop veyon-master.desktop.bak
					mv veyon-configurator.desktop veyon-configurator.desktop.bak
					cd
					echo -e "$COLDEFAUT"
                                        echo "Info : veyon-master et veyon-configurator ont été désactivés sur ce poste."
					echo -e "$COLTXT"
                                fi
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de copie demandée."
				echo -e "$VERT"
				echo "###########################################################################"
				echo ""
				echo "Vous pourrez copier la clé publique veyon plus tard en lançant le script"
				echo "/mnt/netlogon/alancer/copie_clepub_veyon.sh"
				echo ""
				echo "###########################################################################"
				echo -e "$COLINFO"
				echo "A bientôt !"
				echo -e "$COLTXT" 
				exit 0
				;;
			esac

echo "Terminé !"
exit 0
