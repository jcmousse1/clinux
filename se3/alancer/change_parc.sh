#!/bin/bash
#
# change_parc.sh
########################################################################
##### Script permettant de changer de parc un client linux #####
########################################################################

DATESCRIPT="20151209"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "$COLINFO"
echo "Changement de parc..."
echo -e "$COLTXT"
ancienparc=$(cat /etc/parc.txt)

echo -e "$COLDEFAUT"
echo "Parc actuel de la machine : $ancienparc"
echo -e "$COLTXT"

# Choix du nouveau nom de la machine
		echo "Choix du nouveau parc de cette machine :"
		echo ""
		echo "Quel parc choisissez-vous pour cette machine ?"
		read nouvoparc
		echo "Le nouveau parc choisi pour la machine est $nouvoparc"

# Recuperation de la date et de l'heure pour la sauvegarde des fichiers
DATE=$(date +%F+%0kh%0M)

cp /etc/parc.txt /etc/parc_$DATE
echo "$nouvoparc" > /etc/parc.txt

# Fin de la configuration
echo -e "$VERT"
echo "Fin de l'opération."
echo "Le nouveau parc sera pris en compte à la prochaine connexion d'un utilisateur !"
echo -e "$COLTXT"

