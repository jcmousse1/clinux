#!/bin/bash
#
# maj_java.sh
#
# Ce script permet d'installer ou de mettre à jour Java sur les clients linux
# en desactivant temporairement le cache
#
# Version du 20150910

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

rm -f /root/maj_java.sh
apt-get update
echo "#Configuration du proxy pour l'établissement
Acquire::http::Proxy \"http://##ipamon##:3128\";" > /etc/apt/apt.conf.d/99proxy

apt-get remove -y oracle-java7-installer
apt-get install --assume-yes oracle-java8-installer

echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy
