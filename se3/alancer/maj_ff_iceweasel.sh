#!/bin/bash
# **********************************************************
# maj_ff_iceweasel.sh
# Script de rétrogradage de iceweasel-backports en version esr
# 20160316
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est à exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Au cas ou dpkg aurait été interrompu :
dpkg --configure -a

# Test et désinstallation éventuelle de iceweasel-release
if [ -e "/etc/apt/sources.list.d/mozbackports.list" ]; then
echo -e "$COLINFO"
echo "mozbackports.list existe,"
echo -e "$COLTXT"
	test_moz_1=$(cat /etc/apt/sources.list.d/mozbackports.list | grep "^deb http")
	if [ -n "$test_moz_1" ] ; then
	sed -i 's/^deb\ http:\/\/mozilla.debian/#deb\ http:\/\/mozilla.debian/g' /etc/apt/sources.list.d/mozbackports.list
	sed -i 's/iceweasel/firefox/g' /etc/apt/sources.list.d/mozbackports.list
	echo -e "$COLINFO"
	echo "mozbackports.list modifié."
	echo -e "$COLDEFAUT"
	echo "On désinstalle la version actuellement installée..."
	echo -e "$COLTXT"
	apt-get remove -y iceweasel iceweasel-l10n-fr
	else
	echo -e "$COLINFO"
	echo "mozbackports.list a déjà été modifié"
	echo -e "$COLTXT"
	fi
else
	TEST_MOZ=$(cat /etc/apt/sources.list | grep "^deb http://mozilla.debian.net")
	if [ -n "$TEST_MOZ" ] ; then
	sed -i 's/^deb\ http:\/\/mozilla.debian/#deb\ http:\/\/mozilla.debian/g' /etc/apt/sources.list
	sed -i 's/iceweasel/firefox/g' /etc/apt/sources.list
	echo -e "$COLINFO"
	echo "sources.list modifié."
	echo -e "$COLDEFAUT"
	echo "On désinstalle la version actuellement installée..."
	echo -e "$COLTXT"
	apt-get remove -y iceweasel iceweasel-l10n-fr
	else
	echo -e "$COLINFO"
	echo "sources.list a déjà été modifié."
	echo -e "$COLTXT"
	fi
echo -e "$COLINFO"
echo "Rien à faire."
echo -e "$COLTXT"
fi

# Mise à jour des paquets
echo -e "$COLINFO"
echo "Mise à jour des paquets..."
echo -e "$COLTXT"
apt-get update
echo -e "$COLINFO"
echo "Installation ou mise à jour d'iceweasel..."
echo -e "$COLTXT"
apt-get install -y iceweasel iceweasel-l10n-fr
echo -e "$VERT"
echo "Installation terminée."
echo -e "$COLTXT"
