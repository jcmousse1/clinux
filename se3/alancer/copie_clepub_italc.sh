#!/bin/bash
#
###########################################################
# copie_clepub_italc.sh
# Ce script est à lancer sur les clients italc
# Il recupère la clé publique "teacher" du poste "maitre" italc
# 20151207
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

cle=key
repcle=/etc/italc/keys/public/teacher/

echo -e "$JAUNE"
echo "###########################################################################"
echo ""
echo "Voulez-vous configurer italc-client sur ce poste ?"
echo "Il faudra indiquer l'adresse ip du poste maitre italc et son mdp root."
echo ""
echo "###########################################################################"
echo -e "$COLTXT"
echo -e "$COLINFO"
read -p "Que voulez-vous faire ?

1 (je veux configurer italc sur ce client)
2 (non, je veux sortir !) : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du poste italc-master : "
                                echo -e "$COLTXT"
                                read IPMAST
				mv $repcle/$cle $repcle/cle_$DATE1
				cd $repcle
				scp root@$IPMAST:$repcle/$cle . || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie de la clé..."
					echo "Vérifier l'adresse ip du poste italc-master puis relancez le script."
                                        echo -e "$COLTXT"
                                else	
					chown :Eleves $cle
                                        echo -e "$VERT"
                                        echo "La clé a été copiée depuis le poste italc-master $IPMAST"
					echo -e "$COLTXT"
                                fi
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de copie demandée."
				echo -e "$VERT"
				echo "###########################################################################"
				echo ""
				echo "Vous pourrez copier la clé italc plus tard en lançant le script"
				echo "/mnt/netlogon/alancer/copie_clepub_italc.sh"
				echo ""
				echo "###########################################################################"
				echo -e "$COLINFO"
				echo "A bientôt !"
				echo -e "$COLTXT" 
				exit 0
				;;
			esac

echo "Configuration : fichier italc.conf..."
sed -i 's/Authentication=1/Authentication=0/g' /etc/xdg/iTALC\ Solutions/iTALC.conf
echo "Terminé !"
exit 0
