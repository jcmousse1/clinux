#!/bin/bash
# **********************************************************
# maj_java_local_v2.sh
# Script de maj de JRE sur les clients, sans téléchargement
# 20171009
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Nettoyage ancien script local
rm -f /root/maj_java.sh

# Correction du lancement des scripts "unefois" au chargement de lightdm
#echo "#!/bin/bash
#sleep 2
#sh /mnt/netlogon/conex/init_unefois.sh
#/usr/bin/X11/numlockx on
#exit 0" > /usr/bin/init1.sh

# On s'assure que le cache apt est bien présent avant de lancer l'update-upgrade
echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Avec cache" > /etc/monproxy

# On règle les problèmes de mise à jour automatiques inachevées...
dpkg --configure -a
apt-get update
apt-get install -f -y

# Il y a parfois des problèmes avec les màj de vlc...
echo -e "$COLINFO"
echo "Installation ou mise à jour de vlc..."
echo -e "$COLTXT"
apt-get install -y vlc vlc-data
echo -e "$COLINFO"
echo "Installation ou mise à jour de firefox en version esr..."
echo -e "$COLTXT"
apt-get install -y firefox-esr firefox-esr-l10n-fr flashplayer-mozilla
echo -e "$VERT"
echo "Installation de firefox en version esr terminée."
echo -e "$COLTXT"

# Installation ou mise à jour de Java-jdk

# JDK a-t-il deja ete installé sur ce client?
if [ -e /var/cache/oracle-jdk8-installer ];then
	echo -e "$COLDEFAUT"
	echo "Quelle version de jdk voulez-vous installer ?"
	echo ""
	echo "Voici la liste des versions disponibles :"
	ls /mnt/netlogon/alancer | grep jdk-8u
	echo -e "$COLTXT"
	echo -e "$COLINFO"
	echo "N'indiquez que le numéro de version : ex 111 pour la version 8u111 (32 ou 64 bits)"
	echo -e "$COLTXT"
	echo -e "$COLDEFAUT"
	read verjdk
	echo -e "$COLTXT"

#Test architecture 32 ou 64 bits
TEST_ARCH=$(uname -a | grep "i686")
#Test version wheezy (7) ou jessie (8) (obsolète)
#TEST_VERSION=$(cat /etc/debian_version | grep "8.")
# Recuperation de la version de l'archive dans le répertoire local
javaver=$(ls /var/cache/oracle-jdk8-installer | grep jdk-8u | cut -d"-" -f2 | cut -c 3- | awk '{ print $1}')
		if [ "$verjdk" != "$javaver" ]; then
		rm -f /var/cache/oracle-jdk8-installer/jdk-8u*
			if [ ! -n "$TEST_ARCH" ]; then
			echo -e "$COLINFO"
			echo "Mise à jour de jdk..."
			echo "Version 64 bits détectée."
			echo -e "$COLTXT"
			echo "Copie de l'archive jdk 64 bits sur le client..."
			cp /mnt/netlogon/alancer/jdk-8u$verjdk\-linux-x64.tar.gz /var/cache/oracle-jdk8-installer/
			else
			echo -e "$COLINFO"
			echo "Version 32 bits détectée."
			echo -e "$COLTXT"
			echo "Copie de l'archive jdk 32 bits sur le client..."
			cp /mnt/netlogon/alancer/jdk-8u$verjdk\-linux-i586.tar.gz /var/cache/oracle-jdk8-installer/
			fi
			echo -e "$COLINFO"
			echo "Copie terminée."
			echo -e "$COLTXT"
		else
		echo -e "$VERT"
		echo "L'archive d'installation version 8u$verjdk de jdk est déjà présente sur la machine."
		echo -e "$COLTXT"
		fi
	else
	echo -e "$COLDEFAUT"
	echo "java 8 n'est pas encore installé sur ce système."
	echo ""
	echo -e "$COLINFO"
	echo "La première installation prendra un peu de temps..."
	echo -e "$COLTXT"
fi

# On enlève le cache si besoin
echo -e "$COLINFO"
echo "Neutralisation du cache apt..."
echo -e "$COLTXT"
echo "#Configuration du proxy pour l'établissement
Acquire::http::Proxy \"http://##ipamon##:3128\";" > /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Sans cache" > /etc/monproxy

# Installation
apt-get install --assume-yes oracle-java8-installer
echo -e "$COLINFO"

# TEST mise à jour de flashplayer sans cache
#echo "Mise à jour de flashplayer..."
#echo -e "$COLTXT"
#update-flashplugin-nonfree --install
#echo -e "$COLINFO"

# On remet le proxy (cache)
echo -e "$COLINFO"
echo "Rétablissement du cache apt..."
echo -e "$COLTXT"
echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Avec cache" > /etc/monproxy

echo -e "$COLINFO"
echo "JDK a été mis à jour."
echo ""
echo -e "$COLTXT"

# On en profite pour mettre à jour le client
echo -e "$COLINFO"
echo "Mise à jour de la machine..."
echo ""
echo -e "$COLTXT"
apt-get -y upgrade && apt-get -y dist-upgrade

#echo -e "$COLINFO"
#echo "Mise à jour de flashplayer..."
#echo -e "$COLTXT"
#update-flashplugin-nonfree --install
#echo -e "$COLINFO"
#echo "On fait un peu de ménage..."
#echo -e "$COLTXT"
apt-get autoremove -y
apt-get autoclean
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
