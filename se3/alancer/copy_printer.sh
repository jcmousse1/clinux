#!/bin/bash
# **********************************************************
# copy_printer.sh
# Script de copie des pilotes d'impression cups
# (destination depuis source)
# 20180922
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

FTP=https://raw.githubusercontent.com/jcmousse/clinux/master

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Test de l'installation de cups
#TEST_CUPS=$(cat /etc/debian_version | grep "8.")
if [ ! -e "/etc/cups" ]; then
echo -e "$COLDEFAUT"
echo "cups ne semble pas installé sur ce client"
echo "Il faut l'installer avant de poursuivre...."
echo ""
echo -e "$COLTXT"
apt-get update && apt-get install -y cups
echo ""
echo -e "$VERT"
echo "cups a été installé... on poursuit."
echo ""
echo -e "$COLTXT"
fi

# Avertissements initiaux
echo -e "$COLDEFAUT"
echo "Ce script va copier la configuration des imprimantes depuis un autre client."
echo -e "$COLINFO"
echo "Il faudra indiquer l'adresse ip du client source, et entrer son mdp root."
echo ""
echo -e "$COLDEFAUT"
echo "Appuyez sur entrée quand vous voulez!"
echo -e "$COLTXT"
read LULU
echo -e "$COLTXT"

repcups=/etc/cups
fichconf=printers.conf
ppd_dir=ppd

# xxx

echo -e "$JAUNE"
echo "Entrez l'adresse ip du client source : "
echo ""
echo -e "$COLTXT"
read IPCLI

cd /etc/cups

# Copie du fichier /etc/cups/printers.conf ET du répertoire /etc/cups/ppd
	# sauvegarde préalable
mv printers.conf printers.conf.ori
mv ppd ppd.bak
	# copie
scp -r root@$IPCLI:/etc/cups/\{ppd,printers.conf\} . || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo -e "$COLERREUR"
		echo "Erreur lors de la copie des fichiers de configuration depuis $IPCLI..."
		echo "Vérifier l'adresse $IPCLI du client source puis relancez le script."
		exit 1
		echo -e "$COLTXT"
	else
		echo -e "$VERT"
		echo "Les fichiers ont été copiés dans /etc/cups depuis le client $IPCLI"
		echo ""
		echo -e "$COLTXT"
		# Correction des droits
		chown root:lp /etc/cups/printers.conf
		chown -R root:lp /etc/cups/ppd
	fi

# Copie du fichier /etc/cups/printers.conf
#scp root@$IPCLI:$repcups/$fichconf . || ERREUR="1"
#	if [ "$ERREUR" = "1" ];then
#		echo -e "$COLERREUR"
#		echo "Erreur lors de la copie du fichier de configuration sur le client..."
#		echo "Vérifier l'adresse ip du client puis relancez le script."
#		exit 1
#		echo -e "$COLTXT"
#	else
#		echo -e "$VERT"
#		echo "Le fichier printers.conf été copié dans /etc/cups depuis le client $IPCLI"
#		echo ""
#		echo -e "$COLTXT"
#	fi

# Copie du répertoire /etc/cups/ppd
#scp -r root@$IPCLI:$repcups/$ppd_dir . || ERREUR="1"
#	if [ "$ERREUR" = "1" ];then
#		echo -e "$COLERREUR"
#		echo "Erreur lors de la copie du répertoire des pilotes..."
#		echo "Vérifier l'adresse ip du client puis relancez le script."
#		exit 1
#		echo -e "$COLTXT"
#	else
#		echo -e "$VERT"
#		echo "Le répertoire ppd a été copié dans /etc/cups depuis le client $IPCLI"
#		echo ""
#		echo -e "$COLTXT"
#	fi

cd

# Avertissement : Redémarrage nécessaire du service cups
echo -e "$COLINFO"
echo ""
echo "On redémarre le service cups..."
echo -e "$COLTXT"
service cups restart
echo -e "$COLINFO"
echo "Service cups redémarré!"
echo ""
echo -e "$COLTXT"
	
echo -e "$VERT"
echo "Les pilotes d'imprimante ont été installés sur ce client."
echo "Terminé !"
echo ""
echo -e "$COLTXT"
