#!/bin/bash
# **********************************************************
# maj_java_local.sh
# Script de maj de JRE sur les clients, sans téléchargement
# 20160503
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Nettoyage ancien script local
echo -e "$COLINFO"
echo "Nettoyage..."
echo -e "$COLTXT"
rm -f /root/maj_java.sh

# Correction du lancement des scripts "unefois" au chargement de lightdm
echo "#!/bin/bash
sleep 2
sh /mnt/netlogon/conex/init_unefois.sh
/usr/bin/X11/numlockx on
exit 0" > /usr/bin/init1.sh

# Au cas ou dpkg aurait été interrompu :
dpkg --configure -a

# Test et désinstallation éventuelle de iceweasel-release
echo -e "$COLINFO"
echo "Correction éventuelle de la version d'iceweasel..."
echo -e "$COLTXT"

if [ -e "/etc/apt/sources.list.d/mozbackports.list" ]; then
echo -e "$COLINFO"
echo "mozbackports.list existe,"
echo -e "$COLTXT"
	test_moz_1=$(cat /etc/apt/sources.list.d/mozbackports.list | grep "^deb http")
	if [ -n "$test_moz_1" ] ; then
	sed -i 's/^deb\ http:\/\/mozilla.debian/#deb\ http:\/\/mozilla.debian/g' /etc/apt/sources.list.d/mozbackports.list
	sed -i 's/iceweasel/firefox/g' /etc/apt/sources.list.d/mozbackports.list
	echo -e "$COLINFO"
	echo "mozbackports.list modifié."
	echo -e "$COLDEFAUT"
	echo "On désinstalle la version actuellement installée..."
	echo -e "$COLTXT"
	apt-get remove -y iceweasel iceweasel-l10n-fr
	else
	echo -e "$COLINFO"
	echo "mozbackports.list a déjà été modifié"
	echo -e "$COLTXT"
	fi
else
	TEST_MOZ=$(cat /etc/apt/sources.list | grep "^deb http://mozilla.debian.net")
	if [ -n "$TEST_MOZ" ] ; then
	sed -i 's/^deb\ http:\/\/mozilla.debian/#deb\ http:\/\/mozilla.debian/g' /etc/apt/sources.list
	sed -i 's/iceweasel/firefox/g' /etc/apt/sources.list
	echo -e "$COLINFO"
	echo "sources.list modifié."
	echo -e "$COLDEFAUT"
	echo "On désinstalle la version actuellement installée..."
	echo -e "$COLTXT"
	apt-get remove -y iceweasel iceweasel-l10n-fr
	else
	echo -e "$COLINFO"
	echo "sources.list a déjà été modifié."
	echo -e "$COLTXT"
	fi
echo -e "$COLINFO"
echo "Rien à faire."
echo -e "$COLTXT"
fi

# Mise à jour des paquets
echo -e "$COLINFO"
echo "Mise à jour des paquets..."
echo -e "$COLTXT"
apt-get update
echo -e "$COLINFO"
echo "Installation ou mise à jour d'iceweasel..."
echo -e "$COLTXT"
apt-get install -y iceweasel iceweasel-l10n-fr
echo -e "$VERT"
echo "Installation d'iceweasel en version esr terminée."
echo -e "$COLTXT"

# Mise à jour de Java-jdk
#Test architecture 32 ou 64 bits
TEST_ARCH=$(uname -a | grep "i686")
#Test version wheezy (7) ou jessie (8)
TEST_VERSION=$(cat /etc/debian_version | grep "8.")

if [ ! -n "$TEST_ARCH" ]; then
echo -e "$COLINFO"
echo "Mise à jour de jdk..."
echo "Version 64 bits détectée."
echo -e "$COLTXT"
	if [ ! -n "$TEST_VERSION" ]; then
	echo "Copie de l'archive jdk 64 bits sur le client..."
	echo "Version wheezy"
	cp /mnt/netlogon/alancer/jdk-8u101-linux-x64.tar.gz /var/cache/oracle-jdk8-installer/
	else
	echo "Copie de l'archive jdk 64 bits sur le client..."
	echo "Version jessie"
	cp /mnt/netlogon/alancer/jdk-8u101-linux-x64.tar.gz /var/cache/oracle-jdk8-installer/
	fi
else
echo -e "$COLINFO"
echo "Version 32 bits détectée."
echo -e "$COLTXT"
	if [ ! -n "$TEST_VERSION" ]; then
	echo "Copie de l'archive jdk 32 bits sur le client..."
	echo "Version wheezy"
	cp /mnt/netlogon/alancer/jdk-8u101-linux-i586.tar.gz /var/cache/oracle-jdk8-installer/
	else
	echo "Copie de l'archive jdk 32 bits sur le client..."
	echo "Version jessie"
	cp /mnt/netlogon/alancer/jdk-8u101-linux-i586.tar.gz /var/cache/oracle-jdk8-installer/
	fi
fi

# Mise à jour des paquets
echo -e "$COLINFO"
echo "Copie terminée."
#echo "Mise à jour des paquets..."
echo -e "$COLTXT"
#apt-get update

# On enlève le cache si besoin
echo -e "$COLINFO"
echo "Neutralisation du cache apt..."
echo -e "$COLTXT"
echo "#Configuration du proxy pour l'établissement
Acquire::http::Proxy \"http://##ipamon##:3128\";" > /etc/apt/apt.conf.d/99proxy

# Installation
apt-get install --assume-yes oracle-java8-installer

# On remet le proxy (cache)
echo -e "$COLINFO"
echo "Rétablissement du cache apt..."
echo -e "$COLTXT"
echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy

echo -e "$COLINFO"
echo "JDK a été mis à jour."
echo -e "$COLTXT"

# Suppression de java 7 s'il était installé
echo -e "$COLINFO"
echo "Suppression de java 7..."
echo -e "$COLTXT"
apt-get remove -y oracle-java7-installer

# Mise à jour du client et nettoyage
echo -e "$COLINFO"
echo "Mise à jour du client..."
echo -e "$COLTXT"
apt-get upgrade -y
echo -e "$COLINFO"
echo "Mise à jour de flashplayer..."
echo -e "$COLTXT"
update-flashplugin-nonfree --install
echo -e "$COLINFO"
echo "On fait un peu de ménage..."
echo -e "$COLTXT"
apt-get autoremove -y
apt-get autoclean
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
