#!/bin/bash
###########################################################
# install_italc-client.sh
#
# Ce script installe et configure italc-client pour que le client
# utilise la clé publique italc qui a été déposée le se3.
#
# Avant de lancer ce script :
#  - installer italc-master sur un poste du même parc
#  - lancer /root/se3_cree_rep_parc.sh sur le se3
#
# 20151217
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Installation d'italc-client sur le poste
echo -e "$JAUNE"
echo "Installation et configuration d'italc-client sur ce poste..."
echo -e "$COLTXT"
apt-get update
apt-get install -y italc-client

## Configuration d'italc-client ##

# Recherche du parc dans lequel le poste est installé
parc1=$(cat /etc/parc.txt)
echo -e "$JAUNE"
echo "**************************************************************************"
echo "Ce poste est actuellement inscrit dans le parc $parc1"
echo "Est-ce correct ?"
echo -e "$COLTXT"
echo -e "$COLINFO"
echo ""
echo "Si le poste est bien dans le parc $parc1, appuyer sur entrée pour continuer."
echo ""
echo "Sinon, sortez du script avec ctrl-c."
read lulu
echo "**************************************************************************"
echo ""
echo -e "$COLTXT"


echo -e "$JAUNE"
echo "**************************************************************************"
echo "Voulez-vous configurer italc-client sur ce poste ?"
echo ""
echo "Attention, le poste italc-master du parc $parc1 doit être installé au préalable."
echo "Si ce n'est pas fait, quittez ce script. Vous pourrez le relancer plus tard."
echo ""
echo "**************************************************************************"
echo -e "$COLTXT"
echo -e "$COLINFO"
read -p "
1 (je veux configurer italc sur ce client)
2 (non, je veux sortir !) : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				echo -e "$COLINFO"
				echo "Configuration du fichier iTALC.conf..."
				echo -e "$COLTXT"
				# On sauvegarde la configuration d'origine
				cd /etc/xdg/iTALC\ Solutions
				cp iTALC.conf iTALC.conf_$DATE1
				echo -e "$COLINFO"
				echo "Configuration des autorisations dans iTALC.conf..."
				echo -e "$COLTXT"
				sed -i 's/Authentication=1/Authentication=0/g' /etc/xdg/iTALC\ Solutions/iTALC.conf
				echo -e "$COLINFO"
				echo "Configuration du répertoire des clés publiques dans iTALC.conf..."
				echo -e "$COLTXT"
				sed -i 's#^PublicKeyBase.*#PublicKeyBaseDir=\/mnt\/netlogon\/divers\/italc\/'$parc1'\/public#g' /etc/xdg/iTALC\ Solutions/iTALC.conf
				echo -e "$VERT"
				echo "La configuration a été modifiée."
	               		echo -e "$COLTXT"
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de modification demandée."
				echo -e "$COLTXT"
				;;
			esac


# Message de fin
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
exit 0
