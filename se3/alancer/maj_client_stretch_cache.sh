#!/bin/bash
# **********************************************************
# maj_client_stretch_cache.sh
# Script de maj des clients Stretch, avec cache au choix
# 20180914
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$COLDEFAUT"
echo "Ce script est destiné à un système Debian 9 (Stretch)."
echo "Il n'est pas adapté à ce système."
echo -e "$COLTXT"
exit 1
else
echo -e "$VERT"
echo "Debian 9 (Stretch) détectée... on poursuit."
echo -e "$COLTXT"
fi

echo -e "$ROUGE" "Utilisez-vous un cache APT pour les mises à jour ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Cache apt présent pour les màj (cool!)"
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt pour les màj (dommage ;)"
				APTCACHE=n
				break
				;;
			esac
		done
if [ $APTCACHE = "o" ] ; then
	# Définition de l'adresse du proxy apt pour l'installation
	defaultcache="192.168.1.251:3142"
	echo -e "$COLINFO" "Vous utilisez un cache APT pour la mise à jour. " ; tput sgr0
		echo -e "$COLINFO" "Donnez l'adresse IP et le port du cache APT sous la forme 192.168.1.251:3142"
		echo -e "$COLDEFAUT" "Cache par défaut : [$defaultcache]"
		echo "Validez par entrée si cette adresse convient, ou entrez une autre adresse (sans oublier le port) :"
		read APTPROXY
		if [ -z "$APTPROXY" ]; then
		APTPROXY=$defaultcache
		fi
		echo -e "$DEFAUT" "L'adresse IP du cache apt est $APTPROXY"
		echo -e "$COLTXT"
		
	echo ""
	echo "Ajout du proxy apt à la configuration..."
	# Déclaration du proxy pour apt (activation cache, témoin)
	touch /etc/apt/apt.conf.d/99proxy
	echo "#Configuration du proxy apt pour l'installation
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
echo "Avec cache" > /etc/monproxy

else
# Cache désactivé dans la conf apt, témoin positionné
echo "Neutralisation du proxy apt dans la configuration..."
> /etc/apt/apt.conf.d/99proxy
echo "Sans cache" > /etc/monproxy
fi

# On désinstalle java8 qui commence à être lourdaud!
apt-get purge -y oracle-java8-installer

# On règle les problèmes de mise à jour automatiques inachevées...
echo -e "$COLINFO"
echo "Il y a peut-être des màj inachevées..."
echo -e "$COLTXT"
apt-get update
apt-get install -f -y

# Il y a parfois des problèmes avec les màj de vlc
echo -e "$COLINFO"
echo "Mise à jour de vlc..."
echo -e "$COLTXT"
apt-get install -y vlc

# On met à jour les lecteurs flash
echo -e "$COLINFO"
echo "Mise à jour des lecteurs flash pour firefox et chromium..."
echo -e "$COLTXT"
apt-get install -y flashplayer-mozilla flashplayer-chromium

# Changement des sources de geogebra ??
#dpkg -i geogebra-classic_6.0.414.0-201712200847_amd64.deb

# Mise à jour du client
echo -e "$COLINFO"
echo "Mise à jour du client..."
echo -e "$COLTXT"
apt-get upgrade -y && apt-get dist-upgrade -y
echo -e "$COLINFO"
echo "Mise à jour effectuée."
echo ""
echo -e "$COLTXT"

# Nettoyage du poste et rechargement de la liste des paquets
echo -e "$COLINFO"
echo "On fait un peu de ménage..."
echo -e "$COLTXT"
apt-get autoremove -y
apt-get autoclean
apt-get update

# Messages de fin
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
