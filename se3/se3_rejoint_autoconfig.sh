#!/bin/bash
###########################################################
# Ce script est a lancer sur les se3.
# Il télécharge un script d'intégration pour les clients Debian
# et renseigne automatiquement les variables indispensables pour l'intégration.
# Le script obtenu doit copié puis lancé sur les clients à intégrer
# 20150418
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
script1=rejoint_se3_stretch_lxde.sh
script2=rejoint_se3_jessie_lxde.sh
script3=rejoint_se3_jessie_lxde_evo.sh
repscript=/root
dirparams=/etc/clinux
fichparams=paramse3.data
fichproxy=proxys.data

testapt=$(cat $dirparams/$fichproxy | grep ip_cache)
testamon=$(cat $dirparams/$fichproxy | grep ip_amon)
rm -f /root/params*
> $dirparams/$fichparams

cd $repscript
rm -f rejoint_se3_*
rm -f create_machine.sh
wget --no-check-certificate --quiet $ftp/stretch/integration/$script1
#wget --no-check-certificate --quiet $ftp/jessie/integration/$script2
wget --no-check-certificate --quiet $ftp/jessie/integration/$script3
getse3server=$(hostname)
echo "date : $DATE1" > $dirparams/$fichparams
echo "hostname se3 : $getse3server" >> $dirparams/$fichparams
sed -i 's/##se3server##/'$getse3server'/g' $repscript/$script1
#sed -i 's/##se3server##/'$getse3server'/g' $repscript/$script2
sed -i 's/##se3server##/'$getse3server'/g' $repscript/$script3
TEST_ADR=$(ifconfig eth0 | grep "inet adr")
if [ -n "$TEST_ADR" ]; then
# ifconfig contient inet adr
getse3ip=$(ifconfig eth0 | grep inet\ adr | cut -d: -f2 | awk '{ print $1}')
else
# ifconfig contient inet addr
getse3ip=$(ifconfig eth0 | grep inet\ addr | cut -d: -f2 | awk '{ print $1}')
fi
echo "ip se3 : $getse3ip" >> $dirparams/$fichparams
sed -i 's/##se3ip##/'$getse3ip'/g' $repscript/$script1
#sed -i 's/##se3ip##/'$getse3ip'/g' $repscript/$script2
sed -i 's/##se3ip##/'$getse3ip'/g' $repscript/$script3
getpassldap=$(cat /etc/ldap.secret)
echo "pass ldap : $getpassldap" >> $dirparams/$fichparams
sed -i 's/##se3passldap##/'$getpassldap'/g' $repscript/$script1
#sed -i 's/##se3passldap##/'$getpassldap'/g' $repscript/$script2
sed -i 's/##se3passldap##/'$getpassldap'/g' $repscript/$script3
getsuffix=$(cat /etc/samba/smb.conf | grep ldap\ suffix |  cut -c16-)
echo "base dn : $getsuffix" >> $dirparams/$fichparams
sed -i 's/##se3suffix##/'$getsuffix'/g' $repscript/$script1
#sed -i 's/##se3suffix##/'$getsuffix'/g' $repscript/$script2
sed -i 's/##se3suffix##/'$getsuffix'/g' $repscript/$script3
getadmindn=$(cat /etc/samba/smb.conf | grep ldap\ admin\ dn |  cut -c18-)
echo "admin dn : $getadmindn" >> $dirparams/$fichparams
sed -i 's/##se3admindn##/'$getadmindn'/g' $repscript/$script1
#sed -i 's/##se3admindn##/'$getadmindn'/g' $repscript/$script2
sed -i 's/##se3admindn##/'$getadmindn'/g' $repscript/$script3
getdomain=$(cat /etc/samba/smb.conf | grep workgroup |  cut -c14-)
echo "domaine : $getdomain" >> $dirparams/$fichparams
sed -i 's/##se3domain##/'$getdomain'/g' $repscript/$script1
#sed -i 's/##se3domain##/'$getdomain'/g' $repscript/$script2
sed -i 's/##se3domain##/'$getdomain'/g' $repscript/$script3
getrne=$(cat /etc/samba/smb.conf | grep ldap\ suffix |  cut -c19- | cut -c-8)
echo "rne : $getrne" >> $dirparams/$fichparams
sed -i 's/##se3rne##/'$getrne'/g' $repscript/$script1
#sed -i 's/##se3rne##/'$getrne'/g' $repscript/$script2
sed -i 's/##se3rne##/'$getrne'/g' $repscript/$script3
getamon=$(cat $dirparams/$fichproxy | grep ip_amon | cut -d= -f2 | awk '{ print $1}')
sed -i 's/##ipamon##/'$getamon'/g' $repscript/$script3
sed -i 's/##ipamon##/'$getamon'/g' $repscript/$script1
if [ -n "$testapt" ] ; then
getcache=$(cat $dirparams/$fichproxy | grep ip_cache | cut -d= -f2 | awk '{ print $1}')
sed -i 's/##ipcache##/'$getcache'/g' $repscript/$script3
sed -i 's/##ipcache##/'$getcache'/g' $repscript/$script1
else
sed -i 's/##ipcache##/'$getamon'/g' $repscript/$script3
sed -i 's/##ipamon##/'$getamon'/g' $repscript/$script1
fi

# Mise en place des droits sur les fichiers
chmod 700 $repscript/$script1
#chmod 700 $repscript/$script2
chmod 700 $repscript/$script3
chmod 600 $dirparams/$fichparams
chmod 600 $dirparams/$fichproxy

echo -e "$COLINFO"
echo "$repscript/$script1 -> prêt."
echo "$repscript/$script3 -> prêt."
echo ""
echo -e "$COLTXT"
echo -e "$VERT"
echo "Les dernières versions des scripts d'intégration"
echo "ont été téléchargées et configurées pour ce se3."
echo -e "$COLTXT"

cd $repscript
./se3_copie_script_integration.sh

