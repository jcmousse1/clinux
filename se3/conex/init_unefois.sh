#!/bin/bash
#
# init_unefois.sh
# script de lancement des scripts unefois
# 20171214

DATE1=$(date +%F+%0kh%0M)

# On vérifie l'existence de /root/temoins sur le client, on créé le répertoire si besoin
if [ ! -e /root/temoins ]; then
mkdir /root/temoins
fi

# Parcours du répertoire des scripts unefois et lancement de chaque script
cd /mnt/netlogon/once
    for item in *; do
        echo "### $(date) BEGIN ${item}" > /root/temoins/${item}.log
        ./${item}
        echo "### $(date) END   ${item}" >> /root/temoins/${item}.log
    done
exit 0
