#!/bin/bash
###########################################################
# se3_get_java_clinux.sh
# Ce script est a lancer sur les se3.
# Il télécharge dans le répertoire /home/netlogon/clients-linux/alancer/
# les archives tar.gz des installeurs de jre (versions 32 et 64 bits)
# depuis un serveur ftp pour installation sur les clients linux.
# 20160822
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

#ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
ftp=http://jcmousse.free.fr/se3

cd /home/netlogon/clients-linux/alancer
rm -f jdk-8u7* jdk-8u8* jdk-8u9* jdk-8u10*

echo "Voici la liste actuelle des versions disponibles sur ce se3 :"
ls /home/netlogon/clients-linux/alancer | grep jdk-8u

echo "Quelle version de jdk voulez-vous télécharger sur le serveur ?"
read verjdk
echo ""
wget $ftp/jdk-8u$verjdk-linux-x64.tar.gz
wget $ftp/jdk-8u$verjdk-linux-i586.tar.gz
permse3

echo "Terminé!"
echo "Voici la nouvelle liste des versions disponibles sur ce se3 :"
ls /home/netlogon/clients-linux/alancer | grep jdk-8u


