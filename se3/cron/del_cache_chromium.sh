#!/bin/bash
# /usr/share/se3/scripts/del_cache_chromium.sh
# logs : /var/log/del_cache_chromium.log
# Efface le contenu du cache chromium
# A executer en cron une fois par semaine

# On vide le log précédent
> /var/log/del_cache_chromium.log
# On efface le cache chromium dans le profil utilisateur
ls /home | while read A ; do rm -rf /home/$A/.cache/chromium/Default/Cache/* ; done
