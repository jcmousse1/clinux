#!/bin/bash
# /usr/share/se3/scripts/del_cache_ff.sh
# logs : /var/log/del_cache_ff.log
# Efface le contenu du cache ff
# A exécuter en cron chaque semaine

# On vide le log précédent
> /var/log/del_cache_ff.log
# On efface le cache ff dans le profil utilisateur
ls /home | while read A ; do rm -rf /home/$A/.cache/mozilla/firefox/default/cache2/entries/* ; done
