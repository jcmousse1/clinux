#!/bin/bash
# /usr/share/se3/scripts/del_lock_ff.sh
# logs : /var/log/del_lock_ff.log
# Efface les fichiers .parentlock dans les profils firefox
# A executer en cron chaque nuit

# On vide le log de la veille
> /var/log/del_lock_ff.log
# On recherche les fichiers lock pour les effacer
find /home/ -name ".parentlock" -exec rm -vf {} \;

