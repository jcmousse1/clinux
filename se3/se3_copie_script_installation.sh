#!/bin/bash
###########################################################
# se3_copie_script_installation.sh
#
# Ce script est a lancer sur les se3
# Il copie un script d'installation d'un client
# avant l'intégration
# 20170120
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

script1=install_jessie_etabs_evo.sh
script2=install_stretch_etabs.sh
repscript=/home/netlogon/clients-linux/alancer

echo -e "$JAUNE"
echo "###########################################################################"
echo ""
echo "Voulez-vous copier le script d'installation sur un (futur) client Debian ?"
echo "Il faudra donner l'adresse ip du poste à installer, et son mdp root."
echo ""
echo "###########################################################################"
echo -e "$COLTXT"
echo -e "$COLINFO"
read -p "Que voulez-vous faire ?

1 (oui, je veux installer un client JESSIE - Debian 8.x)
2 (oui, je veux installer un client STRETCH - Debian 9.x)
3 (non, j'aime pas les clients linux !) : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client Jessie : "
                                echo -e "$COLTXT"
                                read IPCLI
				scp $repscript/$script1 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie du script sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Le script install_jessie_etabs_evo.sh a été copié dans /root sur le client Jessie à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;
				
				2 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client Stretch : "
                                echo -e "$COLTXT"
                                read IPCLI
				scp $repscript/$script2 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie du script sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Le script install_stretch_etabs.sh a été copié dans /root sur le client Stretch à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de copie demandée."
				echo -e "$COLTXT"
				echo -e "$VERT"
				echo "###########################################################################"
				echo ""
				echo "Vous pourrez copier le script d'intégration plus tard en lançant le script"
				echo "/root/se3_copie_script_installation.sh"
				echo ""
				echo "###########################################################################"
				echo -e "$COLTXT"
				echo -e "$COLINFO"
				echo "A bientôt !"
				echo -e "$COLTXT" ;;
			esac
exit 0
