#!/bin/bash
ROUGE="\\033[1;31m"
DATE=$(date +%F+%0kh%0M)
SERVINV="http://fi-inv.ac-clermont.fr/glpi/plugins/fusioninventory/"

#le tag est constitue du RNE suivi de P (peda) A (admin) M (Management)
# Choix du tag pour l'etablissement
echo -e "$ROUGE" "Il faut affecter un TAG à cette machine" ; tput sgr0
echo -e "$ROUGE" "Le TAG doit etre <RNE> plus une lettre : P (peda), A (admin) ou M (Management)" ; tput sgr0
echo "$ROUGE" "Exemple : 0430020NP pour un poste pedagogique"
echo ""
echo -e "$ROUGE" "Entrez le TAG de l'etablissement : " ; tput sgr0
read TAG
echo -e "$ROUGE" "Entrez le proxy de l'etablissement" ; tput sgr0
echo "sous la forme http://172.17.139.253:3128"
echo "ou <entree> si pas de proxy spécifique "
read PROXY
echo -e "$ROUGE" "Installation de l'agent d'inventaire..." ; tput sgr0
apt-get update
apt-get purge -y ocsinventory-agent
apt-get purge -y fusioninventory-agent
apt-get install -y fusioninventory-agent
cp /etc/default/fusioninventory-agent /etc/default/fusioninventory-agent.ori
sed -i 's/MODE=cron/MODE=daemon/g' /etc/default/fusioninventory-agent
echo "server=$SERVINV"> /etc/fusioninventory/agent.cfg
echo "tag=$TAG">> /etc/fusioninventory/agent.cfg
echo "proxy = $PROXY">> /etc/fusioninventory/agent.cfg
/etc/init.d/fusioninventory-agent restart
# Depose d'un témoin d'installation daté dans /root/
touch /root/fusioninventory-$DATE
echo -e "$ROUGE" "Agent d'inventaire installé !" ; tput sgr0 
