#!/bin/bash
temoin=/root/temoins/temoin.modlogon
# Execution du script unefois
if [ ! -e $temoin ] ; then

# A tester!
#sed -i 's/\/mnt\/netlogon/\/mnt\/netlogon\/conex/g' /etc/gdm3/Init/Default
sed -i '/exit 0/d' /etc/gdm3/Init/Default
sed -i '/netlogon/d' /etc/gdm3/Init/Default
sed -i '/unefois/d' /etc/gdm3/Init/Default
echo "sh /mnt/netlogon/conex/init_unefois.sh
exit 0" >> /etc/gdm3/Init/Default
chmod +x /etc/gdm3/Init/Default
echo "fichier /etc/gdm3/Init/Default modifié" > $temoin

echo "#!/bin/bash
#
# Note that any setup should come before the sessreg command as
# that must be 'exec'ed for the pid to be correct (sessreg uses the parent
# pid)
# Note that output goes into the .xsession-errors file for easy debugging
#
PATH=\"/usr/bin:\$PATH\"
sh /mnt/netlogon/conex/createlinks.sh &
exit 0" > /etc/gdm3/PreSession/Default
chmod +x /etc/gdm3/PreSession/Default
echo "fichier /etc/gdm3/PreSession/Default modifié" >> $temoin

echo "#!/bin/bash
sh /mnt/netlogon/conex/deletelinks.sh
exit 0" > /etc/gdm3/PostSession/Default
echo "fichier /etc/gdm3/PostSession/Default modifié" >> $temoin
fi

