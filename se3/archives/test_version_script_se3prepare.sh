#!/bin/bash

#Couleurs
COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://raw.githubusercontent.com/jcmousse/clinux/master

LINE_TEST()
{
if ( ! wget -q --output-document=/dev/null 'http://wawadeb.crdp.ac-caen.fr/index.html') ; then
	ERREUR "Votre connexion internet ne semble pas fonctionnelle !!" 
	exit 1
fi
}

NODL="no"
DEBUG="no"
#[ -e /root/debug ] && DEBUG="yes"
#[ -e /root/nodl ] && NODL="yes"

LINE_TEST

if [ "$NODL" != "yes" ]; then
	echo -e "$COLTITRE"
	echo "Vérification en ligne que vous avez bien la dernière version du script."
	echo -e "$COLTXT"

	cd /root
	SCRIPT_FILE="se3_prepare_clients_linux.sh"
	SCRIPT_FILE_MD5="se3_prepare_clients_linux.md5"
	SCRIPTS_DIR="/usr/share/se3/sbin"
	
	# Telechargement dans /root du script a jour et du chier md5
	rm -f $SCRIPT_FILE_MD5 $SCRIPT_FILE
	wget --no-check-certificate -N --tries=1 --connect-timeout=1 $ftp/se3/$SCRIPT_FILE || ERREUR="1"
	wget --no-check-certificate -N --tries=1 --connect-timeout=1 $ftp/se3/$SCRIPT_FILE_MD5 || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo "Problème pour récupérer la version en ligne : ABANDON !"
		exit 1	
	fi
	
	# Calcul des md5 des fichiers qu'on vient de telecharger
	MD5_CTRL_FILE=$(cat $SCRIPT_FILE_MD5) # contenu du md5 techarge
	MD5_CTRL_DL=$(md5sum $SCRIPT_FILE) # md5 du script telecharge

	# Controle de ce qu'on vient de telecharger
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_DL" ]
	then	
		echo -e "$COLERREUR"
		echo "Controle MD5 du script téléchargé incorrect, relancez le script pour qu'il soit de nouveau téléchargé"
		echo -e "$COLTXT"
		exit 1
	fi
	
	# On se rend dans /usr/share/se3/sbin
	cd $SCRIPTS_DIR
	
	# Calcul du md5 de /usr/share/se3/sbin/se3_prepare_clients_linux.sh
	MD5_CTRL_LOCAL=$(md5sum $SCRIPT_FILE)

	# On retourne dans /root et on rend le script téléchargé executable
	cd
	chmod +x *.sh

	# On compare le md5 du script présent sur le se3 avec celui du script téléchargé
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_LOCAL" ]; then

		# Si les md5 sont différents
		# On passe la variable relance à yes
		RELANCE="YES"
		# Et on copie le script de /root vers le repertoire des scripts
		cp $SCRIPT_FILE $SCRIPTS_DIR/
	fi

	if [ "$RELANCE" == "YES" ]
	then
		echo -e "$COLINFO"
		echo "Le script a été mis a jour depuis le serveur central, veuiller relancer se3_prepare_clients_linux.sh"
		echo "pour utiliser la nouvelle version"
		sleep 1
		exit 1
		echo -e "$COLTXT"	
	fi

	echo -e "$COLTITRE"
	echo "Vous disposez de la derniere version du script de preparation, on peut poursuivre..."
	sleep 2
	echo -e "$COLTXT"
else
echo "mode debug, pas de telechargement"
sleep 2
fi
