#!/bin/bash
# correctif montage des partages sur les clients linux (ntlmv2 obligatoire)

DATE1=$(date +%F+%0kh%0M)
temoin=/root/temoins/temoin.ntlmv2

[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Execution du script unefois
if [ ! -e $temoin ] ; then
sed -i 's/utf8/utf8,sec=ntlmv2/g' /etc/security/pam_mount.conf.xml
echo "modification ntlmv2 effectuée le $DATE1" > $temoin
fi

