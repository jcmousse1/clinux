#!/bin/bash

# Ce script permet de modifier smb.conf sur les se3 pour les clients linux
# A lancer sur le se3
# Version du 20150623

# Test de la présence du paquet se3-clients-linux
[ ! -e /home/netlogon/clients-linux ] && echo "Le paquet se3-clients-linux n'est pas installé. Installez le et relancez le script." && exit 1

# Modification de smb.conf
TEST_PORT=$(cat /etc/samba/smb.conf | grep "smb ports = 139")
if [ ! -n "$TEST_PORT" ]; then
echo "On modifie smb.conf, si besoin..."
sed -i 's/unix\ extensions\ =\ no/unix\ extensions\ =\ no \n\	smb\ ports\ =\ 139/g' /etc/samba/smb.conf
echo "smb.conf a été modifié."
echo "Les modifications prendront effet après redémarrage du service samba."
else
echo "smb.conf n'avait pas besoin d'être modifié."
fi
echo "Terminé !"
