#!/bin/bash
#
#***********************************************
# mount-a_unefois.sh
# Ajoute le forcage du montage de /mnt/netlogon au démarrage
# 20170116
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.mount-unefois

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	# Modification de /usr/bin/init1.sh pour montage de /mnt/netlogon sur le client
	test_mount=$(cat /usr/bin/init1.sh | grep "mount")
		if [ ! -n "$test_mount" ]; then
		sed -i 's/sleep\ 2/mount\ -a\n\sleep\ 2/g' /usr/bin/init1.sh
		echo "ajout mount -a" > $temoin
		else 
		echo "fichier déjà modifié" $temoin
		fi
	else
	echo "fichier déjà modifié" > $temoin
fi
