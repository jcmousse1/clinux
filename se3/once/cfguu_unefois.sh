#!/bin/bash
#
#***********************************************
# cfguu_unefois.sh
# Configuration des maj auto avec unattended-upgrades
# 20161208
#***********************************************

# Témoin de passage du script sur le client
rm -f /root/temoins/temoin.cfguu*
temoin=/root/temoins/temoin.cfguu4

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
	cd /etc/apt/apt.conf.d
	rm -f 50unattended-upgrades
	TEST_VERSION=$(cat /etc/debian_version | grep "8.")
	if [ ! -n "$TEST_VERSION" ]; then
echo "wheezy"
echo -e "// Automatically upgrade packages from these origin patterns
Unattended-Upgrade::Origins-Pattern {
        // Archive or Suite based matching:
        // Note that this will silently match a different release after
        // migration to the specified archive (e.g. testing becomes the
        // new stable).
	\"o=Debian,a=oldstable\";
	\"o=Debian,a=oldstable-updates\";
	\"o=Debian,a=oldstable-backports\";
	\"o=Debian,a=proposed-updates\";
	\"origin=Debian,archive=oldstable,label=Debian-Security\";
	\"origin=Debian,archive=oldstable,label=Debian-Security\";
	\"origin=Unofficial Multimedia Packages,archive=oldstable,label=Unofficial Multimedia Packages\";
	\"o=Debian Mozilla Team,a=wheezy-backports,l=Debian Mozilla Team,c=firefox-release\";
        \"o=www.geogebra.net,n=stable,l=apt repository,c=main\";
};

// List of packages to not update
Unattended-Upgrade::Package-Blacklist {
//	\"vim\";
//	\"libc6\";
//	\"libc6-dev\";
//	\"libc6-i686\";
};
// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies \"true\";" > /etc/apt/apt.conf.d/50unattended-upgrades

	else

echo "jessie"
echo -e "// Automatically upgrade packages from these origin patterns
Unattended-Upgrade::Origins-Pattern {
        // Archive or Suite based matching:
        // Note that this will silently match a different release after
        // migration to the specified archive (e.g. testing becomes the
        // new stable).
	\"o=Debian,a=stable\";
	\"o=Debian,a=stable-updates\";
	\"o=Debian,a=stable-backports\";
	\"o=Debian,a=proposed-updates\";
	\"origin=Debian,archive=stable,label=Debian-Security\";
	\"origin=Debian,archive=oldstable,label=Debian-Security\";
	\"origin=Unofficial Multimedia Packages,archive=stable,n=jessie,label=Unofficial Multimedia Packages\";
	\"origin=Unofficial Multimedia Packages,archive=stable,n=jessie-backports,label=Unofficial Multimedia Packages\";
	\"o=www.geogebra.net,n=stable,l=apt repository,c=main\";
};

// List of packages to not update
Unattended-Upgrade::Package-Blacklist {
//	\"vim\";
//	\"libc6\";
//	\"libc6-dev\";
//	\"libc6-i686\";
};
// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies \"true\";" > /etc/apt/apt.conf.d/50unattended-upgrades
	fi
	echo "20160318" > $temoin
else
	exit 0
fi

