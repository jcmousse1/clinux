#!/bin/bash
#
#***********************************************
# corrige_smb_unefois.sh
#
# 20170207
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.corrige_smb_unefois

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

NOMMACH=$(hostname)
if [ ! -e $temoin ] ; then
	# Modification du nom de la machine dans smb.conf
	sed -i 's/^netbios name.*/netbios\ name\ =\ '$NOMMACH'/g' /etc/samba/smb.conf
	echo "smb.conf modifié" > $temoin
	else
	exit 0
fi

