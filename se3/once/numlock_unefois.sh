#!/bin/bash
#
#***********************************************
# numlock_unefois.sh
# Rétablit le verrouillage du pavé numérique
# 20160404
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.numlock

# Exécution du script unefois
if [ ! -e $temoin ] ; then
echo "#!/bin/bash
sleep 2
sh /mnt/netlogon/conex/init_unefois.sh
/usr/bin/X11/numlockx on
exit 0" > /usr/bin/init1.sh
	echo "Sasséfait" > $temoin
	else
	exit 0
fi

