#!/bin/bash
#
#***********************************************
# addbackports_unefois.sh
# Ajoute les depots jessie-backports à sources.list
# 20160519
#***********************************************

# Témoin de passage du script sur le client
rm -f /root/temoins/temoin.addbackports

temoin=/root/temoins/temoin.addbackports_1

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	sed -i '/deb\ http:\/\/ftp.debian.org\/debian\/\ jessie-backports/d' /etc/apt/sources.list
	sed -i '/Jessie\ backports/d' /etc/apt/sources.list
	sed -i '/Jessie-backports/d' /etc/apt/sources.list
	sed -i '/^$/d' /etc/apt/sources.list
	echo "#Jessie-backports" >> /etc/apt/sources.list
	echo "deb http://ftp.debian.org/debian/ jessie-backports main contrib non-free" >> /etc/apt/sources.list
	echo "backports ajoutés à sources.list"
	echo "addbackports_unefois.sh exécuté" > $temoin
else
	echo "addbackports a deja été exécuté sur ce poste"
	exit 0
fi

