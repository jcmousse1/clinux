#!/bin/bash
#
#***********************************************
# ffsource_unefois.sh
# Correction des sources firefox/iceweasel
# 20160316
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.ffsources

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	sed -i 's/iceweasel/firefox/g' /etc/apt/sources.list
	sed -i 's/iceweasel/firefox/g' /etc/apt/sources.list.d/mozbackports.list
	echo "Sources ff-iceweasel modifiées" > $temoin
	else
	exit 0
fi

