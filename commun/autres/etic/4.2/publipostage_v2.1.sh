#!/bin/bash

# Script d'élaboration du fichier de publipostage des fiches Etic
#
# --------------------------------------------------------------------
# Ce script est lancé par traitement_fiches_etic*.zsh pour le premier traitement.
# Il peut être relancé seul si les fichiers ont été déplacés après le premier traitement,
# pour corriger le chemin des fichiers dans le fichier csv de publipostage.
# jcm 20171123
# ---------------------------------------------------------------------

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Elaboration du fichier publipostage_etic.csv
fichpubli=nommagepubli.csv

# On vide le fichier temporaire par précaution
> publi2.csv

# Traitement
while IFS=';' read champ1 champ2 champ3 champ4
do line="$champ1;$champ2;$champ3;$champ4"
   echo "$line;ce.$champ1@ac-clermont.fr;$(pwd)/" >> publi2.csv	
done < $fichpubli

# Ajout des noms de fichiers
paste -d '' 'publi2.csv' 'nommagetemp.csv' > publipostage_etic.csv

# Modification de la première ligne
sed -i '/UAI/d' publipostage_etic.csv
sed -i '1iuai;commune;type;nom;mail;fichier' publipostage_etic.csv

# Messages de fin
echo ""
echo -e "$JAUNE"
echo "--------------------------------------------------------------"
echo "Le fichier qui servira au publipostage a été créé sous le nom"
echo 			"publipostage_etic.csv"
echo "--------------------------------------------------------------"
echo -e "$COLTXT"
echo ""
echo -e "$COLINFO"
echo "On fait un peu de ménage..."
# Suppression des fichiers temporaires utilisés pour le traitement
rm -f nommagepdf.csv
rm -f publi2.csv
echo -e "$COLTXT"

echo ""
echo -e "$VERT"
echo "		======================================================"
echo "		******** Tous les traitements sont terminés ! ********"
echo "		======================================================"
echo -e "$COLTXT"
