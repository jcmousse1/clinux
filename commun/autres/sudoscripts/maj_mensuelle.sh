#!/bin/bash
# miseajour - script de mise a jour par l'utilisateur

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"
COLTXT="\033[0;37m"

sudo apt-get update && sudo apt-get -y dist-upgrade && sudo apt-get -y autoremove
echo ""
echo "$VERT"
echo "Mise à jour terminée."
echo "$JAUNE"
echo "Appuyez sur Entrée pour continuer"
echo "$COLTXT"
read lulu
