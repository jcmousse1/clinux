#!/bin/bash
######################################
# install_scratch2_phase1.sh
# Script de préparation à l'installation de scratch2 sur des clients linux debian 64
# Ce script peut être lancé en ssh.
# Il faut finir l'installation à la main sur chaque poste,
# en lançant /mnt/netlogon/alancer/install_scratch2_phase2.sh
# 20161206
######################################
DATE1=$(date +%F+%0kh%0M)

# Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

#ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
#ftp=http://jcmousse.free.fr/se3

echo -e "$COLINFO"
echo "Installation des paquets nécessaires"
echo -e "$COLTXT"
dpkg --add-architecture i386 
apt-get update && apt-get -y upgrade
apt-get install -y libxt6:i386 libnspr4-0d:i386 libgtk2.0-0:i386 libstdc++6:i386 libnss3-1d:i386 lib32nss-mdns libxml2:i386 libxslt1.1:i386 libcanberra-gtk-module:i386 gtk2-engines-murrine:i386 
ln -s /usr/lib/x86_64-linux-gnu/libgnome-keyring.so.0 /usr/lib/libgnome-keyring.so.0
ln -s /usr/lib/x86_64-linux-gnu/libgnome-keyring.so.0.2.0 /usr/lib/libgnome-keyring.so.0.2.0
cd
cp /mnt/netlogon/alancer/AdobeAIRInstaller.bin /tmp/
cp /mnt/netlogon/alancer/Scratch-454.air /tmp/
chmod +x /tmp/AdobeAIR*
chmod +x /tmp/Scratch-*
echo -e "$COLINFO"
echo "La phase 1 de l'installation de Scratch2 est terminée."
echo -e "$COLTXT"
echo -e "$COLDEFAUT"
echo "Appuyez sur Entrée pour poursuivre, ou ctrl-c pour sortir"
echo -e "$COLTXT"
echo ""
read TOTO
cd /tmp
echo -e "$COLINFO"
echo "Eh bien, cliquez maintenant!"
echo -e "$COLTXT"
./AdobeAIRInstaller.bin
echo -e "$COLDEFAUT"
echo "Appuyez sur Entrée pour poursuivre..."
echo -e "$COLTXT"
read LULU
cd /tmp
"Adobe AIR Application Installer" /tmp/Scratch-454.air
mv /usr/share/applications/edu.media.mit.scratch2editor.desktop /usr/share/applications/scratch2.desktop
sed -i 's/^Categories.*/Categories=Application;Education;Development;/g' /usr/share/applications/scratch2.desktop
update-menus
echo -e "$COLINFO"
echo "Raccourcis modifiés."
echo -e "$COLTXT"
echo -e "$VERT"
echo "Installation de scratch2 terminée!"
echo -e "$COLTXT"

