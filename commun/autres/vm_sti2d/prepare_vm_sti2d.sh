#!/bin/bash
#
#
########################################################################
##### Script permettant de preparer un client linux #####
##### Pour la formation en sti2d #####
########################################################################
VERT="\\033[1;32m"
NORMAL="\\033[0;39m"
ROUGE="\\033[1;31m"
ROSE="\\033[1;35m"
BLEU="\\033[1;34m"
BLANC="\\033[0;02m"
BLANCLAIR="\\033[1;08m"
JAUNE="\\033[1;33m"

# Section a completer
DATESCRIPT="20141218"
VERSION="0.1"
##########################################
echo "Preparation du client..."
echo "La version du script utilise est datee du $DATESCRIPT"
echo ""
# Choix du nouveau nom de la machine
		echo -e "$ROUGE" "Choix du nouveau nom de cette machine :" ; tput sgr0
		echo -e "$VERT" "Quel nom choisissez-vous pour cette machine ?"
		echo -e "$JAUNE" "--> ATTENTION : Ni espaces ni accents ! <--"
			read NOMMACH
		echo -e "$VERT" "> Le nom choisi pour la machine est \"$NOMMACH\"." ; tput sgr0
		echo ""
# Choix de l'adresse IP de la machine
		echo -e "$ROUGE" "Choix de l'adresse ip de cette machine :" ; tput sgr0
		echo -e "$VERT" "Quelle adresse ip attribuez-vous à cette machine ? "
		echo -e "$ROUGE" "--> Indiquez seulement le DERNIER octet :"
		echo -e "$JAUNE" "Exemple : 101 pour 192.168.224.101" ; tput sgr0
			read ADRIP
		echo -e "$VERT" "> L'adresse choisie est 192.168.224.$ADRIP" ; tput sgr0
		echo ""
# Choix du nom du nouvel utilisateur
#		echo -e "$ROUGE" "Quel est le nom du nouvel utilisateur ?" ; tput sgr0
#		echo "Entrez un nom pour le nouvel utilisateur,"
#		echo -e "$JAUNE" "ATTENTION : Pas d'espace(s) et pas d'accent(s) !" ; tput sgr0
#			read NOMUSER
#		echo -e "$VERT" "Bienvenue, $NOMUSER" ; tput sgr0
#		echo ""

# Recuperation de la date et de l'heure pour la sauvegarde des fichiers
DATE=$(date +%F+%0kh%0M)

# Configuration du fichier /etc/hosts
echo -e "$ROUGE" "Configuration des fichiers /etc/hostname et /etc/hosts" ; tput sgr0
ANCIENNOM=$(hostname)
cp /etc/hosts /etc/hosts_sauve_$DATE
sed -i 's/'$ANCIENNOM'/'$NOMMACH'/g' /etc/hosts
cp /etc/hostname /etc/hostname_sauve_$DATE
echo "$NOMMACH" > /etc/hostname

echo -e "$VERT" "> Nouveau nom \"$NOMMACH\" mis en place." ; tput sgr0
echo ""
# Configuration de l'adresse IP
echo -e "$ROUGE" "Configuration de l'adresse ip..." ; tput sgr0

cp /etc/network/interfaces /etc/network/interfaces_sauve_$DATE

echo "# The loopback network interface
auto lo
iface lo inet loopback

# eth0
auto eth0
#iface eth0 inet dhcp
iface eth0 inet static
  address 192.168.224.$ADRIP
  network 192.168.224.0
  netmask 255.255.255.0
  broadcast 192.168.224.255
  gateway 192.168.224.254" > /etc/network/interfaces
echo -e "$VERT" "> Nouvelle adresse 192.168.224.$ADRIP configurée." ; tput sgr0
echo ""
# Configuration du DNS
echo "nameserver 192.168.224.254" > /etc/resolv.conf
echo -e "$VERT" "> DNS configuré." ; tput sgr0
echo ""
# Configuration du cache APT
echo "#Configuration du proxy apt LRA
Acquire::http { Proxy \"http://192.168.224.251:3142\"; };" > /etc/apt/apt.conf.d/99proxy
echo -e "$VERT" "> Cache APT configuré." ; tput sgr0
echo ""
# Datage du script (temoin de passage dans /root)
cd /root
rm -f preparation_*
echo "Script de preparation version $VERSION du $DATESCRIPT passe le $DATE" >> /root/preparation_$DATESCRIPT

# Fin de la configuration
echo -e "$VERT" "> Fin de la préparation !" ; tput sgr0
echo ""
echo -e "$ROUGE" "Il faut redemarrer la machine pour prendre en compte les nouveaux paramètres." ; tput sgr0
