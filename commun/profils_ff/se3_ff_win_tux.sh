#!/bin/bash
#
# se3_FF_win_tux.sh
# Ce script synchronise (si besoin) les profils FF linux et windoze
# et remet le profil FF "default" par defaut pour tous les utilisateurs
#
# Dernière modification : 20150101

# Si les répertoires de profil FF windoze ou linux n'existent pas, on les crée
#ls /home | while read A ; do
ls /home|egrep -v "(^_|^netlogon$|^templates$|^aquota.user$|^aquota.group$|^profiles$)"|while read A ; do

if [ ! -e "/home/$A/profil/appdata/Mozilla/Firefox/Profiles/default" ]; then
mkdir -p /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default
fi

if [ ! -e "/home/$A/.mozilla/firefox/default" ]; then
mkdir -p /home/$A/.mozilla/firefox/default
fi

# On synchronise le contenu du profil FF linux avec celui du profil FF windoze
rsync -a /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default/ /home/$A/.mozilla/firefox/default/
rsync -a /home/$A/.mozilla/firefox/default/ /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default/

# Creation ou modification du profiles.ini pour windoze
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=Profiles/default" > /home/$A/profil/appdata/Mozilla/Firefox/profiles.ini

chown -R $A:lcs-users /home/$A/profil/appdata/Mozilla

# Creation ou modification du profiles.ini pour linux
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=default" > /home/$A/.mozilla/firefox/profiles.ini

chown -R $A:lcs-users /home/$A/.mozilla

echo "Profils FF de $A synchronisés"
done
