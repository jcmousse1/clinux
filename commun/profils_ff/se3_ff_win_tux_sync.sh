#!/bin/bash
#
# se3_FF_win_tux_sync.sh
# Ce script synchronise (si besoin) les profils FF linux et windoze
# et remet le profil FF "default" par défaut pour l'utilisateur qui se connecte


# Si les répertoires de profil FF windoze ou linux n'existent pas, on les crée
if [ ! -e "/home/$USER/profil/appdata/Mozilla/Firefox/Profiles/default" ]; then
mkdir -p /home/$USER/profil/appdata/Mozilla/Firefox/Profiles/default
fi

if [ ! -e "/home/$USER/.mozilla/firefox/default" ]; then
mkdir -p /home/$USER/.mozilla/firefox/default
fi

# On synchronise le contenu du profil FF linux avec celui du profil FF windoze
rsync -a /home/$USER/profil/appdata/Mozilla/Firefox/Profiles/default/ /home/$USER/.mozilla/firefox/default/
rsync -a /home/$USER/.mozilla/firefox/default/ /home/$USER/profil/appdata/Mozilla/Firefox/Profiles/default/

if [ ! -e "/home/$USER/profil/appdata/sync_ff.txt" ]; then
# Création ou modification du profiles.ini pour windoze
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=Profiles/default" > /home/$USER/profil/appdata/Mozilla/Firefox/profiles.ini

# Création ou modification du profiles.ini pour linux
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=default" > /home/$USER/.mozilla/firefox/profiles.ini
touch /home/$USER/profil/appdata/sync_ff.txt
fi

chown -R $USER:lcs-users /home/$USER/.mozilla
chown -R $USER:lcs-users /home/$USER/profil/appdata/Mozilla


echo "Profils FF de $USER synchronisés"
done
