#!/bin/bash

# Suppression des liens bureau
#rm -f /home/$USER/Desktop/Classes
#rm -f /home/$USER/Desktop/Partages
#rm -f /home/$USER/Desktop/Mes_Docs

# Suppression des raccourcis copiés à la connexion
ls /home/$USER/link | while read A ; do rm -f /home/$USER/Desktop/$A ; done
ls /home/$USER/link | while read A ; do rm -f /home/$USER/Bureau/$A ; done
sleep 1
rm -f /home/$USER/link/*

sed -i 's/icon/list/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf

killall -u $USER
sleep 1

#rm -f /home/$USER/.mozilla/firefox/default/.parentlock
umount /media/classes
umount /media/partages
umount /home/$USER
exit 0

