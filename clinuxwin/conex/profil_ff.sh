#!/bin/bash
#
# profil_ff.sh
# Script de *test* de synchronisation des profils firefox linux <-> windoze de l'utilisateur
# 20151128
#

timestamp=$(date +%F_%N)

# Test et création du répertoire mozilla tux s'il n'existe pas
if [ ! -e "/home/$USER/.mozilla/firefox" ]; then
echo "Le répertoire .mozilla/firefox n'existe pas, il faut le créer." > /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
mkdir -p /home/$USER/.mozilla/firefox
else echo "Le répertoire .mozilla/firefox existe." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
fi

# Test de l'existence du lien symbolique default ff tux
if [ -L "/home/$USER/.mozilla/firefox/default" ]; then
        echo "Le lien symbolique ff default tux existe." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
else
        # Le lien symbolique n'existe pas, il faut tester la présence d'un répertoire nommé default
        echo "Le lien symbolique ff tux existant n'existe pas." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
        echo "Existe-t-il un répertoire ff default tux ?" >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
        test_default_tux=$(ls /home/$USER/.mozilla/firefox/ | grep -o ^default$)
        if [ -n "$test_default_tux" ]; then
        # default tux existe, il faut le renommer
        echo "Il existe un répertoire default. Il faut le renommer." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
        mv /home/$USER/.mozilla/firefox/default /home/$USER/.mozilla/firefox/default_$timestamp
        echo "Dossier default ff tux existant pour $USER. Le dossier a été renommé." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
        else    
        echo "Pas de répertoire ff default tux pour $USER. On continue." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
        fi
fi

# S'il existe un profil ff linux nommé xxx.default : on le met de coté en le renommant
test_ff_tux=$(ls /home/$USER/.mozilla/firefox/ | grep [.]default$)
if [ -n "$test_ff_tux" ]; then
echo "Il existe un repertoire xxx.default pour ff tux. On le renomme." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
mv /home/$USER/.mozilla/firefox/*.default /home/$USER/.mozilla/firefox/default.old_$timestamp
else
echo "Pas de répertoire xxx.default pour ff tux. On continue." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
fi

# Test de l'existence du répertoire default pour windoze
test_default_win=$(ls /home/$USER/profil/appdata/Mozilla/Firefox/Profiles | grep -o ^default$)
if [ -n "$test_default_win" ]; then
echo "Le dossier default ff pour windoze existe." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
else
echo "Le dossier default ff pour windoze n'existe pas, il faut le créer." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
mkdir -p /home/$USER/profil/appdata/Mozilla/Firefox/Profiles/default
echo "Dossier default ff pour windoze créé." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
fi

# S'il existe un profil ff windoze nommé xxx.default : on le met de coté en le renommant
echo "Il faut renommer un éventuel dossier ff xxx.default windoze..." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
test_ff_win=$(ls /home/$USER/profil/appdata/Mozilla/Firefox/Profiles | grep [.]default$)
if [ -n "$test_ff_win" ]; then
echo "Il exite un dossier ff xxx.default pour windoze, on le renomme." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
cd /home/$USER/profil/appdata/Mozilla/Firefox/Profiles
mv *.default default.old_$timestamp
else
echo "Pas de dossier ff xxx.default pour windoze, on continue." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
fi

# Création du lien symbolique entre les deux dossiers de profil FF s'il n'existe pas
if [ ! -L "/home/$USER/.mozilla/firefox/default" ]; then
echo "Le lien symbolique n'existe pas, il faut le créer." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
ln -s /home/$USER/profil/appdata/Mozilla/Firefox/Profiles/default /home/$USER/.mozilla/firefox/
else
echo "Le lien symbolique existe. On continue." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt
fi

# Création ou modification du profiles.ini pour windoze
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=Profiles/default" > /home/$USER/profil/appdata/Mozilla/Firefox/profiles.ini
echo "profiles.ini ff windoze créé." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt

# Création ou modification du profiles.ini pour linux
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=default" > /home/$USER/.mozilla/firefox/profiles.ini
echo "profiles.ini ff linux créé." >> /home/$USER/profil/appdata/Mozilla/Firefox/logprofil.txt

