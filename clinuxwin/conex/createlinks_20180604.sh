#!/bin/bash

#
# Script exécuté à la connexion de l'utilisateur
#

DATERAPPORT=$(date +%F+%0k:%0M:%S)

#lulu=$(who | grep -v root | cut -d" " -f1)
lili=$(cat /tmp/pamuser.log)

LOG=/tmp/conex_$lili.log

(
echo ""
echo "====================================================================================="
echo "debut de la connexion de $lili : $DATERAPPORT"

# Creation du dossier pour les liens
if [ ! -e "/home/$lili/link" ]; then
mkdir /home/$lili/link
fi
if [ ! -e "/home/$lili/.mozilla" ]; then
#mkdir -p /home/$lili/.mozilla/firefox
mkdir -p /home/$lili/.mozilla
echo "Répertoire FF créé!"
else
echo "Le répertoire FF existe déjà!"
rm -rf /home/$lili/.mozilla/*
fi
# Liens sur le bureau des utilisateurs
#ln -s /media/Professeurs/$lili /home/$lili/Desktop/Documents_perso
ln -s /media/Echange /home/$lili/Desktop/Echange
ln -s /media/Echange /home/$lili/Bureau/Echange

# Copie des raccourcis de base
cp /mnt/netlogon/persolinks/base/*.desktop /home/$lili/Desktop/
cp /mnt/netlogon/persolinks/base/*.desktop /home/$lili/Bureau/
#cp /mnt/netlogon/persolinks/base/*.desktop /home/$lili/Bureau/
cp /mnt/netlogon/persolinks/base/* /home/$lili/link/

# Copie des raccourcis du parc du client
PARC=$(cat /etc/parc.txt)
cp /mnt/netlogon/persolinks/$PARC/*.desktop /home/$lili/Desktop/
cp /mnt/netlogon/persolinks/$PARC/*.desktop /home/$lili/Bureau/
#cp /mnt/netlogon/persolinks/$PARC/*.desktop /home/$lili/Bureau/
cp /mnt/netlogon/persolinks/$PARC/* /home/$lili/link/

# Test et copie des raccourcis de groupe
u=$(groups $lili | grep eleves)
	if [ ! -n "$u" ]; then

        	# Resultat vrai, donc le compte n'appartient pas au groupe eleves.
		echo "$lili n'est pas membre du groupe eleves"

		# On interroge alors le groupe professeurs.
        	v=$(groups $lili | grep professeurs)

		if [ ! -n "$v" ]; then
		# Resultat vrai, donc le compte n'appartient pas au groupe professeurs.
		echo "$lili n'est pas membre du groupe professeurs"
    		else
       		# Sinon résultat faux, le compte appartient au groupe professeurs.
		echo "$lili est membre du groupe professeurs"

		# Copie des raccourcis profs
		cp /mnt/netlogon/persolinks/profs/*.desktop /home/$lili/Desktop/
		cp /mnt/netlogon/persolinks/profs/*.desktop /home/$lili/Bureau/
		#cp /mnt/netlogon/persolinks/profs/*.desktop /home/$lili/Bureau/
		cp /mnt/netlogon/persolinks/profs/* /home/$lili/link/

		# Liens sur le bureau
		ln -s /media/Professeurs/$lili /home/$lili/Desktop/Documents_perso
		ln -s /media/Professeurs/$lili /home/$lili/Bureau/Documents_perso

			# Si veyon-master est installé sur le client, on copie aussi un raccourci veyon-prof sur le bureau
			#if [ -e \"/usr/bin/veyon-master\" ] ; then
			#echo \"veyon-master est installé sur ce poste, on copie le raccourci.\"
			#cp /mnt/netlogon/persolinks/autres/veyon-prof.desktop /home/\$lili/Bureau/
			#cp /mnt/netlogon/persolinks/autres/veyon-prof.desktop /home/\$lili/Desktop/
			#cp /mnt/netlogon/persolinks/autres/veyon-prof.desktop /home/\$lili/links/
			#else
			#echo \"veyon-master n'est pas installé sur ce poste."
			#fi

		# Configuration de FF pour l'utilisateur (prof)
			if [ ! -e "/media/Professeurs/$lili/MonFF" ]; then
			#mkdir -p /media/Professeurs/$lili/MonFF/firefox
			mkdir -p /media/Professeurs/$lili/MonFF
			echo "Répertoire FF perso créé!"
			else
			echo "Le répertoire FF perso existe déjà!"
			rsync -avz /media/Professeurs/$lili/MonFF/.mozilla /home/$lili
			fi
 		# Profil FF
		#echo "[General]
                #StartWithLastProfile=1
                #[Profile0]
                #Name=default
                #IsRelative=0
                #Path=/media/Professeurs/$lili/MonFF/firefox/" > /home/$lili/.mozilla/firefox/profiles.ini

		# Creation d'un dossier pour la synchro des documents
			if [ ! -e "/media/Professeurs/$lili/MesDocs/" ]; then
			mkdir -p /media/Professeurs/$lili/MesDocs
			echo "Répertoire MesDocs créé!"
			else
			echo "Le répertoire MesDocs existe déjà!"
			fi
    		fi
	else
        # Sinon, le compte appartient au groupe eleves.
	echo "$lili est membre du groupe eleves"

	# Copie des raccourcis eleves
	cp /mnt/netlogon/persolinks/eleves/*.desktop /home/$lili/Bureau/
	cp /mnt/netlogon/persolinks/eleves/*.desktop /home/$lili/Desktop/
	cp /mnt/netlogon/persolinks/eleves/* /home/$lili/link/

	# Liens sur le bureau
	ln -s /media/Eleves/$lili /home/$lili/Desktop/Documents_perso
	ln -s /media/Eleves/$lili /home/$lili/Bureau/Documents_perso

	# Configuration de FF pour l'utilisateur (eleve)
		if [ ! -e "/media/Eleves/$lili/MonFF/" ]; then
		#mkdir -p /media/Eleves/$lili/MonFF/firefox
		mkdir -p /media/Eleves/$lili/MonFF
		echo "Répertoire FF perso créé!"
		else
		echo "Le répertoire FF perso existe déjà!"
		rsync -avz /media/Eleves/$lili/MonFF/.mozilla /home/$lili
		fi
	# Profil FF
	chown -R $lili:utilisateurs\ du\ domaine /home/$lili
	#echo "[General]
	#StartWithLastProfile=1
	#[Profile0]
	#Name=default
	#IsRelative=0
	#Path=/media/Eleves/$lili/MonFF/firefox/" > /home/$lili/.mozilla/firefox/profiles.ini

	# Creation d'un dossier pour la synchro des documents
			if [ ! -e "/media/Eleves/$lili/MesDocs/" ]; then
			mkdir -p /media/Eleves/$lili/MesDocs
			echo "Répertoire MesDocs créé!"
			else
			echo "Le répertoire MesDocs existe déjà!"
			fi
	fi
#############################################################################

# Reglage de l'affichage en liste dans les fenetres de pcmanfm
#sed -i 's/icon/list/g' /home/$lili/.config/pcmanfm/LXDE/pcmanfm.conf
#sed -i 's/hidden=1/hidden=0/g' /home/$lili/.config/pcmanfm/LXDE/pcmanfm.conf

# Applications par defaut
#cat /mnt/netlogon/conex/mimeapps.list > /home/$lili/.config/mimeapps.list

# Autres scripts a lancer
#bash /mnt/netlogon/conex/alsavolume.sh
#bash /mnt/netlogon/conex/profil_ff.sh
#bash /mnt/netlogon/conex/clean_vartmp.sh
#bash /mnt/netlogon/conex/warn_overfill.sh

# Mise à jour des menus ?
#update-menus
sleep 1

## Rétablissement des droits sur les répertoires utilisateur
#lulu=$(who | grep -v root | cut -d" " -f1)
chown -R $lili:utilisateurs\ du\ domaine /home/$lili
chown -R $lili:utilisateurs\ du\ domaine /media/Professeurs/$lili
chown -R $lili:utilisateurs\ du\ domaine /media/Eleves/$lili

# Reglage de l'affichage en liste dans les fenetres de pcmanfm
#sed -i 's/icon/list/g' /home/$lili/.config/pcmanfm/LXDE/pcmanfm.conf
#sed -i 's/hidden=1/hidden=0/g' /home/$lili/.config/pcmanfm/LXDE/pcmanfm.conf

# Applications par defaut
#cat /mnt/netlogon/conex/mimeapps.list > /home/$lili/.config/mimeapps.list

# Autres scripts a lancer
bash /mnt/netlogon/conex/alsavolume.sh

) 2>&1 | tee -a $LOG
exit 0
