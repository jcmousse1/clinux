#!/bin/bash
#
# clean_vartmp.sh
# Script de nettoyage du répertoire /var/tmp des clients
# 20170317
#

#timestamp=$(date +%F_%N)

cd /var/tmp
rm -rf audacity-*
rm -rf kdecache-*
exit 0

