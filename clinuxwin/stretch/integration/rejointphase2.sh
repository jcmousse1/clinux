#!/bin/bash

# ****************************************************************
# rejointphase2.sh
# 20170629
# ****************************************************************

DATERAPPORT=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

echo -e "$COLINFO" "Lancement de la jonction du domaine.... "

# Demande d'un ticket kerberos
kdestroy
echo -e "$COLINFO" "Demande d'un ticket Kerberos..."
echo -e "$COLTXT" "Entrez un identifiant d'utilisateur du domaine :"
echo -e "$COLTXT"
read kinituser
#echo -e "$COLTXT" "Entrez le mot de passe pour $kinituser :"
kinit $kinituser
sleep 5
#echo ""

# Verification ticket
klist
echo ""
echo -e "$COLDEFAUT" "Le ticket Kerberos est-il correct ?"
echo -e "$COLINFO" "Si tout va bien, validez par "Entrée"."
read lulu

# Jonction au domaine
echo -e "$COLTXT" "Jonction du domaine : entrez un utilisateur qui a les droits nécessaires :"
echo -e "$COLTXT" "(Il faudra fournir son mot de passe)"
echo -e "$COLTXT"
read joinuser
sleep 3

net ads join -S ##nomad## -U $joinuser
sleep 5

echo -e "$VERT" "Intégration terminée !"
echo -e "$COLTXT"

