!/bin/bash

# ****************************************************************
# prepare_clinuxwin.sh
# 20170607
# ****************************************************************

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
ftp2=http://jcmousse.free.fr

# Test de la connexion internet
LINE_TEST()
{
if ( ! wget -q --output-document=/dev/null 'http://wawadeb.crdp.ac-caen.fr/index.html') ; then
	ERREUR "Votre connexion internet ne semble pas fonctionnelle !"
	exit 1
fi
}

##################################################################
# Verification et téléchargement de la dernière version du script

NODL="no"
DEBUG="no"

# Test de l'accès internet
LINE_TEST

if [ "$NODL" != "yes" ]; then
	echo -e "$COLINFO"
	echo "Vérification en ligne que vous avez bien la dernière version du script..."
	echo -e "$COLTXT"

	cd /root
	SCRIPT_FILE="prepare_clinuxwin.sh"
	SCRIPT_FILE_MD5="prepare_clinuxwin.md5"
	SCRIPTS_DIR="/mnt/netlogon"
	
	# Téléchargement dans /root du script à jour et du fichier md5
	rm -f $SCRIPT_FILE_MD5 $SCRIPT_FILE
	wget --no-check-certificate -N --tries=1 --connect-timeout=1 --quiet $ftp/clinuxwin/$SCRIPT_FILE || ERREUR="1"
	wget --no-check-certificate -N --tries=1 --connect-timeout=1 --quiet $ftp/clinuxwin/$SCRIPT_FILE_MD5 || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo -e "$COLERREUR"
		echo "Problème pour récupérer la version en ligne : ABANDON !"
		echo -e "$COLTXT"
		exit 1	
	fi
	
	# Calcul des md5 des fichiers qu'on vient de télécharger
	MD5_CTRL_FILE=$(cat $SCRIPT_FILE_MD5) # contenu du md5 téléchargé
	MD5_CTRL_DL=$(md5sum $SCRIPT_FILE) # md5 du script téléchargé

	# Controle de ce qu'on vient de télécharger
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_DL" ]
	then	
		# Il y a une erreur dans ce qui a été téléchargé
		echo -e "$COLERREUR"
		echo "Controle MD5 du script téléchargé incorrect. Relancez le script pour qu'il soit de nouveau téléchargé."
		echo -e "$COLTXT"
		exit 1
	fi

	# On se rend dans /usr/share/se3/sbin
	cd $SCRIPTS_DIR

	# Calcul du md5 de /usr/share/se3/sbin/se3_prepare_clients_linux.sh
	MD5_CTRL_LOCAL=$(md5sum $SCRIPT_FILE)

	# On retourne dans /root et on rend le script téléchargé executable
	cd
	chmod +x *.sh

	# On compare le md5 du script présent sur le se3 avec celui du script téléchargé
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_LOCAL" ]; then

		# Si les md5 sont différents, on passe la variable relance à yes
		RELANCE="YES"
		# Puis on copie le script depuis /root vers le repertoire des scripts
		cp $SCRIPT_FILE $SCRIPTS_DIR/
	fi

	if [ "$RELANCE" == "YES" ]
	then
		echo -e "$JAUNE"
		echo "Le script a été mis à jour depuis le dépôt."
		echo "Relancez prepare_clinuxwin.sh pour utiliser la nouvelle version."
		echo ""
		sleep 1
		exit 1
		echo -e "$COLTXT"
	fi

	echo -e "$VERT"
	echo ""
	echo "Vous disposez de la dernière version du script, on peut continuer..."
	sleep 3
	echo -e "$COLTXT"
else
echo "Mode debug, pas de téléchargement."
sleep 2
fi
###########################################################
# Définition des variables du domaine

echo -e "$COLINFO" "Entrez l'adresse IP du serveur des partages: " ; tput sgr0
read IPPART
if [ -z "$IPPART" ]; then
echo "erreur de saisie!"
fi
echo -e "$COLTXT" "L'adresse IP du serveur des partages est $IPPART"
echo "Adresse serveur partages : $IPPART" > /home/jc/clinux/clinuxwin/params.txt

echo -e "$COLINFO" "Entrez l'adresse IP du serveur DNS: "
echo -e "$COLDEFAUT" "IP par défaut : [$IPPART]"
echo -e "$COLINFO" "Validez par entrée si cette adresse convient, ou entrez une autre adresse:"
echo -e "$COLTXT"
read IPDNS
	if [ -z "$IPDNS" ]; then
	IPDNS=$IPPART
	fi
echo -e "$COLTXT" "L'adresse IP du serveur DNS est $IPDNS "
echo "Adresse serveur dns : $IPDNS" >> /home/jc/clinux/clinuxwin/params.txt

echo -e "$COLINFO" "Entrez le nom de domaine (en minuscules): " ; tput sgr0
echo -e "$COLINFO" "Exemple : acad-clermont.local " ; tput sgr0
read DOM1
if [ -z "$DOM1" ]; then
echo "erreur de saisie!"
fi
echo -e "$COLTXT" "Le domaine est $DOM1 "
echo "Nom de domaine : $DOM1" >> /home/jc/clinux/clinuxwin/params.txt

DOM2=${DOM1^^}

echo -e "$COLINFO" "Entrez le nom DNS du serveur : " ; tput sgr0
echo -e "$COLINFO" "Exemple : 04309999k-ad-1 " ; tput sgr0
read NOMDNS
if [ -z "$NOMDNS" ]; then
echo "erreur de saisie!"
fi
echo -e "$COLTXT" "Le nom dns du serveur est $NOMDNS "
echo -e "$COLTXT" "Le nom fqdn du serveur est $NOMDNS.$DOM1 "
echo "Nom dns du serveur : $NOMDNS" >> /home/jc/clinux/clinuxwin/params.txt


sed -i s/##ippart##/$IPPART/g /home/jc/clinux/clinuxwin/sedtest.txt
sed -i s/##ipdns##/$IPDNS/g /home/jc/clinux/clinuxwin/sedtest.txt
sed -i s/##dom1##/$DOM1/g /home/jc/clinux/clinuxwin/sedtest.txt
sed -i s/##dom2##/$DOM2/g /home/jc/clinux/clinuxwin/sedtest.txt
sed -i s/##nomdns##/$NOMDNS/g /home/jc/clinux/clinuxwin/sedtest.txt

echo -e "$COLINFO" "Les variables ont été remplacées. "
echo -e "$VERT" "Terminé ! "
echo -e "$COLTXT"


