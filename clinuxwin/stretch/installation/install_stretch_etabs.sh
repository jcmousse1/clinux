#!/bin/bash
# **********************************************************
# Installation automatisée d'un client stretch
# après installation de base du système.
# Installation de lxde s'il n'est pas installé.
# auteur jc mousseron 12/2017
# Ce script est distribué selon les termes de la licence GPL
# version du 20171221
# **********************************************************
DATEINST=$(date +%F+%0kh%0M)
FTP=https://raw.githubusercontent.com/jcmousse/clinux/master
FTP2=http://jcmousse.free.fr

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Test pour les distraits
[ -e /home/netlogon/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# On neutralise le cache apt utilisé pendant l'installation de base du système
> /etc/apt/apt.conf
> /etc/apt/apt.conf.d/99proxy

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 9 (Stretch)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 9 (Stretch) détectée... on poursuit."
fi

TEST_ROCHE=$(cat /etc/profile | grep "139.253")
if [ ! -n "$TEST_ROCHE" ]; then
	echo -e "$JAUNE" "Le proxy http n'est pas renseigné dans /etc/profile" ; tput sgr0
	echo -e "$ROUGE" "Lieu d'installation : Etes-vous au lycée Roche-Arnaud ? [o/N] " ; tput sgr0
	read replra
		if [ ! "$replra" = "o" -a ! "$replra" = "oui" ]; then
		echo "Installation dans un établissement standard."
		echo "Poursuivons..."
		ROCHE=n
		else
		echo "Installation au lycée Roche-Arnaud."
		ROCHE=o
		fi


		if [ $ROCHE = "o" ] ; then
		# On declare le proxy http (ip du se3) dans /etc/profile si on installe depuis Roche-Arnaud,
		# puis on sort du script : il faut se relogger pour prendre en compte le proxy.
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		echo "export http_proxy=\"http://172.17.139.253:3128\"" >> /etc/profile
		echo "export https_proxy=\"https://172.17.139.253:3128\"" >> /etc/profile
		echo "Le proxy http a été renseigné dans /etc/profile."
		echo "Le script va s'interrompre..."
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 3
		exit
		else
		# Sinon on continue
		echo "On poursuit..." 
		fi
else
# On vérifie le lieu d'installation
echo -e "$COLDEFAUT" "Le proxy du lycée Roche-Arnaud a été détecté dans la configuration du client. " ; tput sgr0
echo -e "$ROUGE" "Lieu d'installation : Etes-vous bien au lycée Roche-Arnaud ? [O/n] " ; tput sgr0
echo ""
	read replra
		if [ ! "$replra" = "n" -a ! "$replra" = "non" ]; then
		echo "Installation à Roche-Arnaud." ; tput sgr0
		echo "Poursuivons..." ; tput sgr0
		ROCHE=o
		else
		echo "Installation dans un autre bahut." ; tput sgr0
		echo ""
		ROCHE=n
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		> /etc/apt/apt.conf
		> /etc/apt/apt.conf.d/99proxy

		echo -e "$COLINFO" "Le proxy http a été supprimé dans /etc/profile."
		echo -e "$COLINFO" "Le script va s'interrompre..."
		echo ""
		echo -e "$COLTXT" 
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 3
		exit
		fi
 
fi

echo -e "$ROUGE" "Utilisez-vous un cache APT pour l'installation ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Cache apt présent pour l'installation (cool!)"
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt pour l'installation (dommage ;)"
				APTCACHE=n
				break
				;;
			esac
		done

if [ $APTCACHE = "o" ] ; then
	# Définition de l'adresse du proxy apt pour l'installation
	defaultcache="172.17.139.251:3142"
	echo -e "$COLINFO" "Vous utilisez un cache APT pour l'installation. " ; tput sgr0
		echo -e "$COLINFO" "Donnez l'adresse IP et le port du cache APT sous la forme 192.168.143.240:3142"
		echo -e "$COLDEFAUT" "Cache par défaut : [$defaultcache]"
		echo "Validez par entrée si cette adresse convient, ou entrez une autre adresse (sans oublier le port) :"
		read APTPROXY
		if [ -z "$APTPROXY" ]; then
		APTPROXY=$defaultcache
		fi
		echo -e "$DEFAUT" "L'adresse IP du cache apt est $APTPROXY"
		echo -e "$COLTXT"
		
	echo ""
	#echo "Ajout temporaire du proxy apt à la configuration..."
	# Déclaration du proxy pour apt
	#touch /etc/apt/apt.conf.d/99proxy
	#echo "#Configuration du proxy apt pour l'installation
#Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
#else
#> /etc/apt/apt.conf.d/99proxy
fi


# Désactivation de l'entrée cle usb dans /etc/fstab si le systeme a ete installé avec une clé
sed -i 's/\/dev\/sdb/#\/dev\sdb/g' /etc/fstab

echo -e "$ROUGE" "C'est parti!" ; tput sgr0
echo -e "$JAUNE" "En fonction de la connexion internet, l'installation prendra de 30 à 90 minutes. " ; tput sgr0
echo -e "$JAUNE" "Soyez patient(e)..." ; tput sgr0
echo ""
echo -e "$VERT" "Installation de lxde..." ; tput sgr0
apt-get update
apt-get install --assume-yes lxde

echo -e "$VERT" "Modifications des sources..." ; tput sgr0
cp /etc/apt/sources.list /etc/apt/sources.save

echo "deb http://debian.proxad.net/debian/ stretch main contrib non-free
deb-src http://debian.proxad.net/debian/ stretch main contrib non-free

deb http://security.debian.org/ stretch/updates main contrib non-free
deb-src http://security.debian.org/ stretch/updates main contrib non-free

# Stretch-updates, previously known as 'volatile'
deb http://debian.proxad.net/debian/ stretch-updates main contrib non-free
deb-src http://debian.proxad.net/debian/ stretch-updates main contrib non-free

# Stretch-backports
# deb http://ftp.debian.org/debian/ stretch-backports main contrib non-free" > /etc/apt/sources.list

echo "# Deb-multimedia
deb ftp://ftp.deb-multimedia.org stretch main non-free
#deb ftp://ftp.deb-multimedia.org stretch-backports main" > /etc/apt/sources.list.d/deb-multimedia.list

echo "# Java installer
#deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main
#deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" > /etc/apt/sources.list.d/webupd8team-java.list

# Ajout du dépot pour geogebra 5
echo "# Geogebra
deb http://www.geogebra.net/linux/ stable main" > /etc/apt/sources.list.d/geogebra.list


echo -e "$VERT" "Sources modifiées, mise à jour des paquets..." ; tput sgr0
apt-get update
apt-get install --assume-yes dirmngr
# Ajout de clés pour les dépots supplémentaires
echo -e "$VERT" "Installation des clés des nouveau dépots" ; tput sgr0
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C072A32983A736CF
apt-get install --allow-unauthenticated --assume-yes deb-multimedia-keyring
apt-get update

echo -e "$VERT" "Modification de lxsession-logout" ; tput sgr0
echo "Récupération des sources..."
cd
wget --no-check-certificate $FTP2/stretch/lxsession-0.5.3.tar.xz
echo -e "$VERT" "Décompression de l'archive" ; tput sgr0
tar -Jxvf lxsession-0.5.3.tar.xz

echo -e "$VERT" "Modification de lxsession-logout.c" ; tput sgr0
cd /root/lxsession-0.5.3/lxsession-logout/
rm -f lxsession-logout.c
wget --no-check-certificate $FTP2/stretch/lxsession-logout.c

if [ $APTCACHE = "o" ] ; then
echo "Ajout temporaire du proxy apt à la configuration..."
	# Déclaration du proxy pour apt
	touch /etc/apt/apt.conf.d/99proxy
	echo "#Configuration du proxy apt pour l'installation
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
apt-get update
else
> /etc/apt/apt.conf.d/99proxy
fi

echo -e "$VERT" "Installation des outils de compilation" ; tput sgr0
apt-get install --assume-yes gcc intltool pkg-config xorg-dev glib-2.0 libdbus-glib-1-dev libspice-client-gtk-3.0-dev libpolkit-agent-1-dev valac libunique-dev
echo -e "$VERT" "Installation et compilation des modifications" ; tput sgr0
cd /root/lxsession-0.5.3
./configure
make && make install
rm -rf /root/lxsession*

export DEBIAN_FRONTEND=noninteractive

echo -e "$ROUGE" "On commence une longue série d'installations...." ; tput sgr0
echo ""
apt-get install debconf

#echo -e "$VERT" "installation de java 8 derniere version" ; tput sgr0
#echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 boolean true" | debconf-set-selections

if [ $APTCACHE = "o" ] ; then
echo -e "$JAUNE" "Neutralisation du proxy APT de l'installation" ; tput sgr0
sed -i 's/Acquire/#Acquire/g' /etc/apt/apt.conf.d/99proxy
fi
#apt-get install --assume-yes oracle-java8-installer
echo "rien"
if [ $APTCACHE = "o" ] ; then
echo -e "$JAUNE" "Rétablissement du proxy APT de l'installation" ; tput sgr0
sed -i 's/#Acquire/Acquire/g' /etc/apt/apt.conf.d/99proxy
fi

echo -e "$VERT" "Installation de libreoffice" ; tput sgr0
apt-get install --assume-yes libreoffice libreoffice-l10n-fr
echo -e "$VERT" "Installation de firefox esr" ; tput sgr0
apt-get install --assume-yes firefox-esr firefox-esr-l10n-fr
echo -e "$VERT" "Installation d'un tas de trucs divers et variés..." ; tput sgr0
echo -e "$JAUNE" "Il y en a pour un moment, vous pouvez aller boire un coup!" ; tput sgr0
apt-get install --assume-yes flashplayer-mozilla vlc gimp scribus audacity lame
apt-get install --assume-yes gstreamer1.0-fluendo-mp3 ffmpeg sox twolame vorbis-tools faad totem
TEST_VERSION=$(uname -a | grep "i686")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$VERT" "Version 64 bits détectée" ; tput sgr0
apt-get install --assume-yes w64codecs
else
echo -e "$VERT" "Version 32 bits détectée" ; tput sgr0
apt-get install --assume-yes w32codecs
fi
apt-get install --assume-yes alsa-base alsa-utils alsamixergui alsa-oss ntpdate ethtool nictools-pci screen apt-listchanges xinit
apt-get install --assume-yes ttf-mscorefonts-installer libdvdcss2 evince anacron unrar tightvncserver net-tools
apt-get install --assume-yes chromium chromium-l10n flashplayer-chromium
apt-get install --assume-yes kdenlive kde-l10n-fr okular ksnapshot
apt-get install -y --allow-unauthenticated geogebra5
apt-get install --assume-yes firmware-linux-nonfree
echo -e "$VERT" "Ouf, les installations sont terminées !" ; tput sgr0
echo ""
chmod 755 /etc/X11/xinit/xinitrc
echo -e "$JAUNE" "On fait un peu de ménage..." ; tput sgr0
apt-get --assume-yes autoremove
apt-get clean
apt-get autoclean

#echo -e "$VERT" "Desactivation des autres os dans Grub" ; tput sgr0
#chmod -x /etc/grub.d/30_os-prober
#update-grub

# Installation et configuration des mises à jour automatiques
#if [ $MAJAUTO = "o" ] ; then
echo ">>>> Installation des maj automatiques <<<<<"
apt-get install --assume-yes unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $FTP/stretch/50unattended-upgrades
echo -e "$VERT" "Mises à jour automatiques installées et configurées" ; tput sgr0
#fi

#echo -e "$VERT" "Modifications pour le démarrage a distance...." ; tput sgr0
#echo -e "$VERT" "Désactivation de NETDOWN dans /etc/init.d/halt" ; tput sgr0
#sed -i 's/NETDOWN=yes/NETDOWN=no/g' /etc/init.d/halt
#echo -e "$VERT" "Activation de la fonction WOL sur la carte réseau" ; tput sgr0
ethtool -s eth0 wol g
#sed -i '/exit 0/d' /etc/rc.local
#echo "ethtool -s eth0 wol g"  >> /etc/rc.local
#echo "exit 0" >> /etc/rc.local

# Configuration du clavier
sed -i 's/us/fr/g' /etc/default/keyboard

# Chagement des mots de passe par défaut
echo -e "$ROUGE" "Il est temps de changer le mots de passe de root" ; tput sgr0
echo -e "$JAUNE" "Entrez (2 fois) le nouveau mot de passe pour root" ; tput sgr0
passwd
#echo -e "$ROUGE" "Il faut aussi changer le mots de passe de l'utilisateur maintenance" ; tput sgr0
#echo -e "$JAUNE" "Entrez (2 fois) le nouveau mot de passe pour maintenance" ; tput sgr0
#passwd maintenance

# Script de renommage
cd /root
rm -f renomme_client*
wget --no-check-certificate $FTP/se3/alancer/renomme_client_linux_v2.sh
chmod +x renomme_client_linux_v2.sh

# Datage du script (temoin de passage dans /root)
cd /root
rm -f installation_*
echo "Installation le $DATEINST" >> /root/installation_$DATEINST

echo -e "$JAUNE" "Neutralisation du proxy APT de l'installation" ; tput sgr0
sed -i 's/Acquire/#Acquire/g' /etc/apt/apt.conf.d/99proxy

# Messages de fin d'installation
echo ""
echo -e "$VERT" "La configuration automatique est terminée." ; tput sgr0
echo ""
echo -e "$JAUNE" "Pensez à faire les operations qui manquent :" ; tput sgr0
echo -e "$JAUNE" "- dépot de la clé ssh publique" ; tput sgr0
echo -e "$JAUNE" "- configuration et lancement du script d'integration" ; tput sgr0
echo ""
echo "Vous pouvez maintenant rebooter la machine Stretch!"
