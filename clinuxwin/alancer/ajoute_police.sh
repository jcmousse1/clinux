#!/bin/bash
#
# ajoute_police.sh
########################################################################
##### Script permettant d'ajouter des polices sur un client linux #####
########################################################################

DATESCRIPT="20171215"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /home/netlogon/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "$COLINFO"
echo "Ajout de polices de caractères sur le client..."
echo -e "$COLTXT"

# Script d'ajout des nouvelles polices
ls /mnt/netlogon/divers/fonts | while read A ; do cp "/mnt/netlogon/divers/fonts/$A" /usr/local/share/fonts/ ; done

# Mise à jour du cache des polices sur le client
fc-cache -v

# Fin de la configuration
echo -e "$VERT"
echo "Fin de l'opération."
echo -e "$COLTXT"

