#!/bin/bash

# ****************************************************************
# prepare_clinuxwin.sh
# Script principal de préparation des scripts pour les clients linux
# A lancer sur clinux-serv
# 20171222
# ****************************************************************

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
ftp2=http://jcmousse.free.fr

# Récupération de l'ip du serveur
#getse3ip=$(ifconfig $(ifconfig | grep enp | cut -d: -f1) | grep "inet\ 1" |awk '{print $2}')

#####

# Fonction test de la connexion
LINE_TEST()
{
if ( ! wget -q --output-document=/dev/null 'http://wawadeb.crdp.ac-caen.fr/index.html') ; then
	ERREUR "Votre connexion internet ne semble pas fonctionnelle !"
	exit 1
fi
}

#####

###################################################################
# Verification et téléchargement de la dernière version du script #
###################################################################

NODL="no"
DEBUG="no"
# lili

# Test de l'accès internet
LINE_TEST

# Test et téléchargement éventuel d'une nouvelle version
if [ "$NODL" != "yes" ]; then
	echo -e "$COLINFO"
	echo "Vérification en ligne que vous avez bien la dernière version du script..."
	echo -e "$COLTXT"

	cd /root
	SCRIPT_FILE="prepare_clinuxwin_20171207.sh"
	SCRIPT_FILE_MD5="prepare_clinuxwin_20171207.md5"
	SCRIPTS_DIR="/home/netlogon/clients-linux"
	
	# Téléchargement dans /root du script à jour et du fichier md5
	rm -f $SCRIPT_FILE_MD5 $SCRIPT_FILE
	wget --no-check-certificate --no-cache -N --tries=1 --connect-timeout=1 --quiet $ftp/clinuxwin/$SCRIPT_FILE || ERREUR="1"
	wget --no-check-certificate --no-cache -N --tries=1 --connect-timeout=1 --quiet $ftp/clinuxwin/$SCRIPT_FILE_MD5 || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo -e "$COLERREUR"
		echo "Problème pour récupérer la version en ligne : ABANDON !"
		echo -e "$COLTXT"
		exit 1	
	fi
	
	# Calcul des md5 des fichiers qu'on vient de télécharger
	MD5_CTRL_FILE=$(cat $SCRIPT_FILE_MD5) # contenu du md5 téléchargé
	MD5_CTRL_DL=$(md5sum $SCRIPT_FILE) # md5 du script téléchargé

	# Controle de ce qu'on vient de télécharger
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_DL" ]
	then	
		# Il y a une erreur dans ce qui a été téléchargé
		echo -e "$COLERREUR"
		echo "Controle MD5 du script téléchargé incorrect. Relancez le script pour qu'il soit de nouveau téléchargé."
		echo -e "$COLTXT"
		exit 1
	fi

	# On se rend dans /home/netlogon/clients-linux
	cd $SCRIPTS_DIR

	# Calcul du md5 de /home/netlogon/clients-linux/prepare_clinuxwin.sh
	MD5_CTRL_LOCAL=$(md5sum $SCRIPT_FILE)

	# On retourne dans /root et on rend le script téléchargé executable
	cd
	chmod +x *.sh

	# On compare le md5 du script présent sur le serveur clinux avec celui du script téléchargé
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_LOCAL" ]; then

		# Si les md5 sont différents, on passe la variable relance à yes
		RELANCE="YES"
		# Puis on copie le script depuis /root vers le repertoire des scripts
		cp $SCRIPT_FILE $SCRIPTS_DIR/
	fi

	if [ "$RELANCE" == "YES" ]
	then
		echo -e "$JAUNE"
		echo "Le script a été mis à jour depuis le dépôt."
		echo "Relancez /home/netlogon/clients-linux/prepare_clinuxwin.sh"
		echo ""
		echo -e "$COLTXT"
		sleep 1
		exit 1	
	fi

	echo -e "$VERT"
	echo ""
	echo "Vous disposez de la dernière version du script, on peut continuer..."
	sleep 3
	echo -e "$COLTXT"
else
echo "Mode debug, pas de téléchargement."
sleep 2
fi

##############################################################################

# Fonction mise à jour de la machine et installation des paquets indispensables
MAJSERV()
{
apt-get update && apt-get -y upgrade && apt-get -y dist-upgrade
apt-get install -y samba cifs-utils apt-cacher-ng net-tools
}

##########################

# Fonction affichage du fichier de paramètres proxy(s)
AFFICHE_PROXYS()
{
cat /etc/clinux/proxys.data
}

##########################

# Fonction affichage du fichier de paramètres serveur
AFFICHE_PARAMS()
{
cat /etc/clinux/params.txt
}

##########################

# Fonction définition des variables du domaine
DEF_VAR()
{
# Fichier de paramètres
#params_file=/home/jc/clinux/clinuxwin/sedtest.txt
params_file=/etc/clinux/params.txt

# Recup des parametres existants
EX_IPPART=$(cat /etc/clinux/params.txt | grep partages | cut -d':' -f2)
EX_IPDNS=$(cat /etc/clinux/params.txt | grep dns | cut -d':' -f2)
EX_DOM1=$(cat /etc/clinux/params.txt | grep domaine | cut -d':' -f2)
EX_NOMDNS=$(cat /etc/clinux/params.txt | grep rodc | cut -d':' -f2)
EX_NOMAD=$(cat /etc/clinux/params.txt | grep pdc | cut -d':' -f2)

echo -e "$JAUNE"
echo "####################################"
echo "# Saisie des paramètres du serveur #"
echo "####################################"
echo -e "$COLTXT"

echo -e "$COLTITRE" "Adresse IP du serveur des partages" ; tput sgr0
echo -e "$COLDEFAUT" "Adresse existante dans le fichier de paramètres : [$EX_IPPART] " ; tput sgr0
echo -e "$COLINFO" "Validez par entrée si cette adresse convient, ou entrez une autre adresse : " ; tput sgr0
echo -e "$COLTXT"
read IPPART
echo ""
if [ -z "$IPPART" ]; then
IPPART=$EX_IPPART
fi
echo -e "$COLTXT" "L'adresse IP du serveur des partages est $IPPART"
echo ""
echo "Adresse serveur partages :$IPPART" > $params_file

echo -e "$COLTITRE" "Entrez l'adresse IP du serveur DNS: "
echo -e "$COLDEFAUT" "Adresse existante dans le fichier de paramètres : [$EX_IPDNS] "
echo -e "$COLINFO" "Validez par entrée si cette adresse convient, ou entrez une autre adresse:"
echo -e "$COLTXT"
read IPDNS
	if [ -z "$IPDNS" ]; then
	IPDNS=$EX_IPDNS
	fi
echo -e "$COLTXT" "L'adresse IP du serveur DNS est $IPDNS "
echo ""
echo "Adresse serveur dns :$IPDNS" >> $params_file

echo -e "$COLTITRE" "Entrez le nom de domaine (en minuscules): " ; tput sgr0
echo -e "$COLINFO" "Exemple : acad-clermont.local " ; tput sgr0
echo -e "$COLDEFAUT" "Domaine existant dans le fichier de paramètres : [$EX_DOM1] "
echo -e "$COLINFO" "Validez par entrée si ce nom convient, ou entrez une autre adresse:"
echo -e "$COLTXT"
echo ""
read DOM1
echo ""
	if [ -z "$DOM1" ]; then
	DOM1=$EX_DOM1
	fi
echo -e "$COLTXT" "Le domaine est $DOM1 "
echo ""
echo "Nom de domaine :$DOM1" >> $params_file

echo -e "$COLTITRE" "Entrez le nom DNS du serveur : " ; tput sgr0
echo -e "$COLDEFAUT" "Nom DNS existant dans le fichier de paramètres : [$EX_NOMDNS] "
echo -e "$COLINFO" "Validez par entrée si ce nom convient, ou entrez une autre adresse:"
echo -e "$COLTXT"
read NOMDNS
	if [ -z "$NOMDNS" ]; then
	NOMDNS=$EX_NOMDNS
	fi
echo ""
echo -e "$COLTXT" "Le nom du serveur rodc est $NOMDNS "
echo -e "$COLTXT" "Le nom fqdn du serveur est $NOMDNS.$DOM1 "
echo "Serveur rodc :$NOMDNS" >> $params_file
echo "Nom fqdn du serveur :$NOMDNS.$DOM1" >> $params_file
echo ""
echo -e "$COLTITRE" "Entrez le nom du serveur PDC : " ; tput sgr0
echo -e "$COLDEFAUT" "Nom existant dans le fichier de paramètres : [$EX_NOMAD] "
echo -e "$COLINFO" "Validez par entrée si ce nom convient, ou entrez une autre adresse:"
echo -e "$COLTXT"
read NOMPDC
	if [ -z "$NOMPDC" ]; then
	NOMPDC=$EX_NOMAD
	fi
echo -e "$COLTXT" "Le nom du serveur PDC est $NOMPDC "
echo "Nom pdc :$NOMPDC" >> $params_file

echo -e "$JAUNE"
echo "##### Paramètres serveur #####"
echo ""
echo "Voici les paramètres actuels pour le serveur :"
echo ""
echo -e "$COLTXT"
AFFICHE_PARAMS
echo ""
echo -e "$COLINFO"
echo "Validez par entrée si tout est correct, ou quittez le script (ctrl-c) puis relancez le."
echo -e "$COLTXT"
read lapin
echo ""
}

##########################

# Fonction création des répertoires indispensables
DEF_REP_1()
{
echo -e "$COLINFO"
echo "Création des répertoires indispensables..."
echo -e "$COLTXT"

rep1=/etc/clinux
rep2=/home/netlogon/clients-linux/alancer
rep3=/home/netlogon/clients-linux/once
rep4=/home/netlogon/clients-linux/conex
rep5=/home/netlogon/clients-linux/persolinks/base
rep6=/home/netlogon/clients-linux/persolinks/cdi
rep7=/home/netlogon/clients-linux/persolinks/sprofs
rep8=/home/netlogon/clients-linux/persolinks/eleves
rep9=/home/netlogon/clients-linux/persolinks/profs
rep10=/home/netlogon/clients-linux/persolinks/autres
rep11=/home/netlogon/clients-linux/divers

	if [ ! -e $rep1 ]; then
	mkdir -p /etc/clinux
	fi
	if [ ! -e $rep2 ]; then
	mkdir -p /home/netlogon/clients-linux/alancer
	fi
	if [ ! -e $rep3 ]; then
	mkdir -p /home/netlogon/clients-linux/once
	fi
	if [ ! -e $rep4 ]; then
	mkdir -p /home/netlogon/clients-linux/conex
	fi
	if [ ! -e $rep5 ]; then
	mkdir -p /home/netlogon/clients-linux/persolinks/base
	fi
	if [ ! -e $rep6 ]; then
	mkdir -p /home/netlogon/clients-linux/persolinks/cdi
	fi
	if [ ! -e $rep7 ]; then
	mkdir -p /home/netlogon/clients-linux/persolinks/sprofs
	fi
	if [ ! -e $rep8 ]; then
	mkdir -p /home/netlogon/clients-linux/persolinks/eleves
	fi
	if [ ! -e $rep9 ]; then
	mkdir -p /home/netlogon/clients-linux/persolinks/profs
	fi
	if [ ! -e $rep10 ]; then
	mkdir -p //home/netlogon/clients-linux/persolinks/autres
	fi
	if [ ! -e $rep11 ]; then
	mkdir -p /home/netlogon/clients-linux/divers
	fi
}

##########################

# Fonction affichage du fichier de paramètres proxy(s)
CONF_PROXYS()
{
echo -e "$JAUNE"
echo "####################################"
echo "# Saisie des paramètres des proxys #"
echo "####################################"
echo -e "$COLTXT"

touch /etc/clinux/proxys.data
dirprox=/etc/clinux
fichproxy=proxys.data
testapt=$(cat $dirprox/$fichproxy | grep ip_cache)
testamon=$(cat $dirprox/$fichproxy | grep ip_amon)

if [ ! -n "$testapt" ] ; then
echo -e "$COLTITRE"
echo "Y a-t-il un cache apt dans l'etablissement ?"
echo -e "$COLTXT"
		PS3='Repondre par o ou n: '
		LISTE=("[o] oui" "[n]  non")
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo -e "$COLINFO"
				echo "Il y a un cache apt dans cet etablissement (cool)."
				echo -e "$COLTXT"
				CACHE=o
				break
				;;
				2|n)
				echo -e "$COLINFO"
				echo "Pas de cache apt ici (beurk)."
				echo -e "$COLTXT"
				echo ""
				CACHE=n
				break
				;;
			esac
done
else CACHE=o
fi

	# Définition de l'adresse du cache apt de l'etablissement
if [ $CACHE = "o" ] ; then
	if [ ! -n "$testapt" ] ; then
		echo -e "$COLTITRE"
		echo "Adresse du CACHE APT de l'etablissement :"
		echo -e "$COLINFO"
		echo "Entrez l'adresse IP du cache APT sous la forme 172.17.31.251"
		echo -e "$COLTXT"
		echo ""
		read APTCACHE
		echo "ip_cache=$APTCACHE" >> $dirprox/$fichproxy
		echo -e "$COLINFO" "ip du cache stockée dans le fichier" ; tput sgr0
		echo -e "$COLTXT"
		echo "L'adresse du CACHE APT est $APTCACHE"
		echo -e "$COLTXT"
		echo ""
	else		
		APTCACHE=$(cat $dirprox/$fichproxy | grep ip_cache | cut -d= -f2 | awk '{ print $1}')
		echo -e "$COLTXT" "valeur lue pour le cache : $APTCACHE " ; tput sgr0
	fi
else
	if [ $CACHE = "o" ] ; then
	# On affiche l'adresse ip du cache
	echo -e "$COLTXT"
	echo "Voici l'adresse du cache apt enregistrée sur ce serveur :"
	echo -e "$testapt"	
	fi
fi

	# Définition de l'adresse du proxy amon de l'etablissement
	if [ ! -n "$testamon" ] ; then
		echo -e "$COLINFO" "Adresse du proxy AMON de l'etablissement :"
		echo -e "$COLTITRE"
		echo "Entrez l'adresse du proxy Amon de l'etablissement sous la forme 172.17.31.254"
		echo -e "$COLTXT"
		read IPAMON
		echo ""
		echo "ip_amon=$IPAMON" >> $dirprox/$fichproxy
		echo -e "$COLINFO" "ip de l'Amon stockée dans le fichier" ; tput sgr0
		echo -e "$COLTXT"
		echo "L'adresse du proxy Amon est $IPAMON"
		echo -e "$COLTXT"
		echo ""
	else
		IPAMON=$(cat $dirprox/$fichproxy | grep ip_amon | cut -d= -f2 | awk '{ print $1}')
		echo -e "$COLTXT" "valeur lue pour l'amon : $IPAMON " ; tput sgr0
	fi
}

####

# Fonction configuration du serveur (minimal) samba

CONF_SMB()
{
cp /etc/samba/smb.conf /etc/samba/smb.conf_$DATE1
dom1=$(cat /etc/clinux/params.txt | grep domaine | cut -d':' -f2)
name=$(hostname)
echo "#smb.conf minimal
# Partage pour les clients

[global]
workgroup = $dom1
netbios name = $name
dns proxy = no
domain logons = no
domain master = no

[netlogon]
comment = Scripts clients linux
path = /home/netlogon/clients-linux
browseable = yes
read only = yes
guest ok = yes" > /etc/samba/smb.conf
}

###

# Fonction réglage des droits

PERM()
{
echo -e "$COLINFO" "Réglage des droits sur les scripts... fait."
echo ""
echo -e "$COLTXT"
chmod +x /home/netlogon/clients-linux/*.sh
chmod +x $rep2/*.sh
chmod +x $rep2/*.sh
chmod +x $rep2/*.sh
}

###############################################################
###############################################################

# Création des répertoires
DEF_REP_1

# Configuration des proxys
CONF_PROXYS
echo ""
AFFICHE_PROXYS
echo ""
echo -e "$COLTITRE"
echo "Les paramètres PROXYs sont-ils corrects? "
echo -e "$COLTXT"
		read -p "	o (oui)
	n (non) : " rep

	case $rep in
			o )
	# On met les bonnes valeurs dans les scripts
		cd $rep2
		rm -f $rep2/maj_java*
		rm -f $rep2/maj_client_stretch*
		#####
		;;
			n )
		> /etc/clinux/proxys.data
		CONF_PROXYS
		;;
	esac

echo -e "$COLINFO"
echo "##############################################################"
echo "##############################################################"
echo -e "$COLTXT"

# Test existence fichier paramètres
if [ -f "/etc/clinux/params.txt" ];then
echo -e "$COLINFO"
echo "Le fichier de paramètres serveur existe."
echo -e "$COLTXT"
else
DEF_VAR
fi

# Affichage des paramètres actuels
echo -e "$JAUNE"
echo "##############################"
echo "###  Paramètres SERVEUR    ###"
echo "##############################"
echo ""
echo "Voici les paramètres actuels pour le serveur :"
echo ""
echo -e "$COLTXT"
AFFICHE_PARAMS
echo ""

echo -e "$COLTITRE"
echo "Les paramètres SERVEUR sont-ils corrects? "
echo -e "$COLTXT"
		read -p "	o (oui)
	n (non) : " rep

	case $rep in
			o )
	# Tout va bien, on continue
		echo "On poursuit..."		
		#cd $rep2
		#rm -f $rep2/maj_java*
		#rm -f $rep2/maj_client_stretch*
		#####
		;;
			n )
		echo ""
		DEF_VAR
		;;
	esac
echo ""

# Mise à jour de la machine et installations
echo ""
echo -e "$COLINFO"
echo "Mise à jour de la machine..."
echo -e "$COLTXT"
MAJSERV
# Configuration de samba
echo ""
echo -e "$COLINFO"
echo "Configuration de samba pour le partage des clients..."
echo -e "$COLTXT"
CONF_SMB

###################################################
## Telechargement et configuration des scripts ##
script_connect=createlinks_20171212.sh
script_deconnect=deletelinks_20171212.sh

# Scripts de connexion/déconnexion (repertoire /conex)
cd $rep4
rm -f init_unefois*
wget --no-check-certificate --quiet $ftp/clinuxwin/conex/init_unefois.sh
echo -e "$COLINFO"
echo "/conex/init_unefois.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f createlinks*
wget --no-check-certificate --quiet $ftp/clinuxwin/conex/$script_connect
echo -e "$COLINFO"
echo "/conex/createlinks.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f deletelinks*
wget --no-check-certificate --quiet $ftp/clinuxwin/conex/$script_deconnect
echo -e "$COLINFO"
echo "/conex/deletelinks.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f profil_ff*
wget --no-check-certificate --quiet $ftp/clinuxwin/conex/profil_ff.sh
echo -e "$COLINFO"
echo "/conex/profil_ff.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f alsavolume*
wget --no-check-certificate --quiet $ftp/clinuxwin/conex/alsavolume.sh
echo -e "$COLINFO"
echo "/conex/alsavolume.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f mimeapps.list*
wget --no-check-certificate --quiet $ftp/clinuxwin/conex/mimeapps.list
echo -e "$COLINFO"
echo "/conex/mimeapps.list créé ou mis à jour."

chmod +x *.sh
chmod 755 *.sh

# Scripts à exécuter unefois (repertoire /once)
cd $rep3
rm -f test_unefois.sh modif_lxde_icons.sh lxde_icons_unefois.sh cfguu_unefois.sh xscreen_unefois.sh mount-a_unefois.sh
rm -f test_unefois* lxde_icons_unefois* config_wol_unefois* cfguu_unefois*
wget --no-check-certificate --quiet $ftp/clinuxwin/once/test_unefois.sh
wget --no-check-certificate --quiet $ftp/clinuxwin/once/lxde_icons_unefois.sh
#wget --no-check-certificate --quiet $ftp/clinuxwin/once/cfguu_unefois.sh
wget --no-check-certificate --quiet $ftp/clinuxwin/once/xscreen_unefois.sh
#wget --no-check-certificate --quiet $ftp/clinuxwin/once/mount-a_unefois.sh

chmod +x *.sh
chmod 755 *.sh

# Scripts qui peuvent être lancés sur les clients (répertoire /alancer)
# Récupération de l'ip du serveur
getse3ip=$(ifconfig $(ifconfig | grep enp | cut -d: -f1) | grep "inet\ 1" |awk '{print $2}')

cd $rep2
wget --no-check-certificate --quiet $ftp/clinuxwin/alancer/maj_java_local.sh
		wget --no-check-certificate --quiet $ftp/clinuxwin/alancer/maj_client_stretch.sh
	if [ -n "$testapt" -o $CACHE = "o" ] ; then
		sed -i 's/##ipcache##/'"$APTCACHE"'/g' $rep2/maj_java_local.sh
		sed -i 's/##ipamon##/'"$IPAMON"'/g' $rep2/maj_java_local.sh
		sed -i 's/##ipcache##/'"$APTCACHE"'/g' $rep2/maj_client_stretch.sh
		sed -i 's/##ipamon##/'"$IPAMON"'/g' $rep2/maj_client_stretch.sh
		echo -e "$COLINFO"
		echo "maj_java_local.sh et maj_client_stretch.sh téléchargés ou mis à jour. "
		echo -e "$COLTXT"
		rm -f change_proxy_client.sh
		wget --no-check-certificate --quiet $ftp/clinuxwin/alancer/change_proxy_client.sh
		sed -i 's/##ipcache##/'"$APTCACHE"'/g' $rep2/change_proxy_client.sh
		sed -i 's/##ipamon##/'"$IPAMON"'/g' $rep2/change_proxy_client.sh
		echo -e "$COLINFO"
		echo "change_proxy_client.sh téléchargé ou mis à jour. "
		echo -e "$COLTXT"
	else
		sed -i 's/##ipcache##/'"$IPAMON"'/g' $rep2/maj_java*
		sed -i 's/##ipamon##/'"$IPAMON"'/g' $rep2/maj_java*
		sed -i 's/3142/3128/g' $rep2/maj_java*
		sed -i 's/##ipcache##/'"$IPAMON"'/g' $rep2/maj_client_*
		sed -i 's/##ipamon##/'"$IPAMON"'/g' $rep2/maj_client_*	

		echo -e "$COLINFO"
		echo "maj_java_local.sh et maj_client_stretch.sh téléchargés ou mis à jour. "
		echo -e "$COLTXT"
	fi

	# Scripts de correction des clés ssh, de renommage et d'installation
cd $rep2
rm -f $rep2/correctif_cles_ssh*
wget --no-check-certificate --quiet $ftp/clinuxwin/alancer/correctif_cles_ssh.sh
sed -i 's/##ipamon##/'"$IPAMON"'/g' $rep2/correctif_cles_ssh.sh
sed -i 's/##se3ip##/'"$getse3ip"'/g' $rep2/correctif_cles_ssh.sh
echo -e "$COLINFO"
echo "correctif_cles_ssh.sh téléchargé ou mis à jour. "
echo -e "$COLTXT"
rm -f $rep2/renomme_client_linux*
wget --no-check-certificate --quiet $ftp/clinuxwin/alancer/renomme_client_linux_v2.sh
#sed -i 's/##se3rne##/'"$getrne"'/g' $rep2/renomme_client_linux_v2.sh
echo -e "$COLINFO"
echo "renomme_client_linux_v2.sh téléchargé ou mis à jour. "
echo -e "$COLTXT"
rm -f $rep2/corrige_uuid_swap*
wget --no-check-certificate --quiet $ftp/clinuxwin/alancer/corrige_uuid_swap.sh
echo -e "$COLINFO"
echo "corrige_uuid_swap.sh téléchargé ou mis à jour. "
echo -e "$COLTXT"
rm -f $rep2/xscreensaver_*
wget --quiet $ftp2/se3/xscreensaver_5.36-1_amd64.deb
wget --quiet $ftp2/se3/xscreensaver_5.36-1_i386.deb
echo -e "$COLINFO" "Paquets xscreensaver téléchargés." ; tput sgr0
cd
echo ""
echo -e "$VERT" "Variables remplacées dans les scripts." ; tput sgr0
cd

# Recuperation et configuration du script d'integration des clients
script_int=rejoint_clinux_win_auto_20171205.sh

cd /home/netlogon/clients-linux
rm -f rejoint_clinux*
wget --no-check-certificate --quiet $ftp/clinuxwin/stretch/integration/$script_int

IPPART=$(cat /etc/clinux/params.txt | grep partages | cut -d':' -f2)
IPDNS=$(cat /etc/clinux/params.txt | grep dns | cut -d':' -f2)
DOM1=$(cat /etc/clinux/params.txt | grep domaine | cut -d':' -f2)
NOMDNS=$(cat /etc/clinux/params.txt | grep rodc | cut -d':' -f2)
NOMAD=$(cat /etc/clinux/params.txt | grep pdc | cut -d':' -f2)
echo ""
echo "partages : $IPPART"
echo "dns : $IPDNS"
echo "domaine : $DOM1"
echo "nom dns rodc : $NOMDNS"
echo "nom pdc : $NOMAD"
echo "ip serveur clinux : $getse3ip"
echo ""
echo -e "$COLINFO" "Validez par entrée si tout est correct."
echo -e "$COLTXT"
read zut

sed -i 's/##ippart##/'"$IPPART"'/g' /home/netlogon/clients-linux/$script_int
sed -i 's/##ipdns##/'"$IPDNS"'/g' /home/netlogon/clients-linux/$script_int
sed -i 's/##dom1##/'"$DOM1"'/g' /home/netlogon/clients-linux/$script_int
sed -i 's/##nomdns##/'"$NOMDNS"'/g' /home/netlogon/clients-linux/$script_int
sed -i 's/##nomad##/'"$NOMAD"'/g' /home/netlogon/clients-linux/$script_int
sed -i 's/##se3_ip##/'"$getse3ip"'/g' /home/netlogon/clients-linux/$script_int

echo -e "$VERT" "Les scripts d'intégration sont prets. "
echo ""
echo -e "$COLTXT"

# Recuperation et configuration du script d'installation des clients
script_install=install_stretch_etabs.sh
cd /home/netlogon/clients-linux
rm -f install_stretch_etabs*
wget --no-check-certificate --quiet $ftp/clinuxwin/stretch/installation/$script_install

echo -e "$VERT" "Le script d'installation a été téléchargé. "
echo ""
echo -e "$COLTXT"

# Téléchargement des raccourcis usuels
cd $rep5
rm -f iceweasel.desktop
rm -f repare_profil.desktop
rm -f ent.desktop
wget --no-check-certificate --quiet $ftp/clinuxwin/raccourcis/iceweasel.desktop
wget --no-check-certificate --quiet $ftp/clinuxwin/raccourcis/repare_profil.desktop
wget --no-check-certificate --quiet $ftp/clinuxwin/raccourcis/ent.desktop
cd $rep8
rm -f eleves.desk*
wget --no-check-certificate --quiet $ftp/clinuxwin/raccourcis/eleves.desktop
cd $rep9
rm -f prof.desk*
wget --no-check-certificate --quiet $ftp/clinuxwin/raccourcis/prof.desktop
#cd $rep10
#rm -f veyon-prof*
#wget --no-check-certificate --quiet $ftp/clinuxwin/raccourcis/veyon-prof.desktop
echo -e "$VERT"
echo "Téléchargement des raccourcis terminé."
echo -e "$COLTXT"
echo -e "$COLINFO"
echo "Ajustement des droits sur les répertoires et les fichiers..."
echo -e "$COLTXT"

# Recuperation et configuration du script de copie sur les clients (integration)
script_copie_integ=copie_script_integration_clinuxwin.sh

cd /home/netlogon/clients-linux
rm -f copie_script_integration*
wget --no-check-certificate --quiet $ftp/clinuxwin/$script_copie_integ

# Recuperation et configuration du script de copie sur les clients (installation)
script_copie_install=copie_script_installation_clinuxwin.sh

cd /home/netlogon/clients-linux
rm -f copie_script_installation*
wget --no-check-certificate --quiet $ftp/clinuxwin/$script_copie_install

# Réglage des droits sur les répertoires et les scripts
PERM

# Dépot d'un témoin dans /root
rm -f /root/prep_*
touch /root/prep_$DATE1
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
exit 0
