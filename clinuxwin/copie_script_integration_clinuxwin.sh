#!/bin/bash
###########################################################
# Ce script est a lancer sur les serveur clinuxwin
# Il copie un script d'intégration sur le client (Stretch)
# pour permettre l'intégration
# 20171221
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

script1=rejoint_clinux_win_auto_20171205.sh
script2=rejoint_clinux_win_auto_20171205.sh
script3=a
repscript=/home/netlogon/clients-linux

echo -e "$JAUNE"
echo "##################################################################"
echo ""
echo "Voulez-vous copier le script sur un client pour l'intégrer ? "
echo ""
echo "Il faudra indiquer l'adresse ip du client à intégrer et son mdp root."
echo ""
echo "##################################################################"
echo ""
echo -e "$COLTXT"
echo -e "$COLTITRE"
read -p "Que voulez-vous faire ?

1 (oui, je veux intégrer un client STRETCH - Debian 9.x)
2 (oui, je veux intégrer un client ZOUZOU - Debian 45.x)
3 (non, j'ai du lait sur le gaz !) : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client Stretch : "
                                echo -e "$COLTXT"
                                read IPCLI
				scp $repscript/$script1 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie du script sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Le script $script1 a été copié dans /root sur le client Stretch à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;

				2 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client ZOUZOU : "
                                echo -e "$COLTXT"
                                read IPCLI
				scp $repscript/$script2 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie du script sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Le script $script2 a été copié dans /root sur le client Zouzou à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de copie du script d'intégration sur le client."
				echo -e "$COLTXT"
				echo -e "$VERT"
				echo "##########################################################################"
				echo ""
				echo "Vous pourrez copier le script d'intégration sur les clients en lançant le script"
				echo "/home/netlogon/clients-linux/copie_script_integration_clinuxwin.sh"
				echo ""
				echo "##########################################################################"
				echo -e "$COLTXT"
				echo ""
				echo -e "$COLINFO"
				echo "A bientôt !"
				echo -e "$COLTXT" ;;
			esac
exit 0
