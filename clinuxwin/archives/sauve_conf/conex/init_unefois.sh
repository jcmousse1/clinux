#!/bin/bash
#
DATE1=$(date +%F+%0kh%0M)

# On vérifie l'existence de /root/temoins sur le client, on créé le répertoire si besoin
if [ ! -e /root/temoins ]; then
mkdir /root/temoins
fi

# Parcours du répertoire des scripts unefois et lancement de chaque script
cd /mnt/netlogon/once
    for item in * ; do
        echo "### $(date) BEGIN ${item}" > /root/temoins/log.${item}
        ./${item}
        echo "### $(date) END   ${item}" >> /root/temoins/log.${item}
    done
exit 0
